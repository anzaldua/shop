<?php


	class ControladorUsuarios{

			/*=============================================
			=         REGISTRO DE USUARIO          =
			=============================================*/

      	public function ctrRegistroUsuario(){

      		if(isset($_POST["regUsuario"])){

      			if(preg_match('/^[a-zA-ZñÑáéíóúÁÉÍÓÚ ]+$/', $_POST["regUsuario"]) &&
				   preg_match('/^[^0-9][a-zA-Z0-9_]+([.][a-zA-Z0-9_]+)*[@][a-zA-Z0-9_]+([.][a-zA-Z0-9_]+)*[.][a-zA-Z]{2,4}$/', $_POST["regEmail"]) &&
				   preg_match('/^[a-zA-Z0-9]+$/', $_POST["regPassword"])){

      				$encriptar = crypt($_POST["regPassword"], '$2a$07$asxx54ahjppf45sd87a5a4dDDGsystemdev$');

      				$encriptarEmail = md5($_POST["regEmail"]);

				   	$datos = array("nombre" => $_POST["regUsuario"],
				   				   "password" => $encriptar,
								   "email" => $_POST["regEmail"],
								   "foto" => "",
								   "modo" => "directo",
							       "verificacion" => 1,
							       "emailEncriptado" => $encriptarEmail);

			       $tabla = "usuarios";

			       $respuesta = ModeloUsuarios::mdlRegistroUsuario($tabla, $datos);

			       if($respuesta == "ok"){

				       	/*=============================================
				       	=    VERIFICACION DE CORREO ELECTRONICO      =
				       	=============================================*/

				       	date_default_timezone_get("America/Mexico_City");

				       	$url = Ruta::ctrRuta();

				       	$mail = new PHPMailer;

				       	$mail ->Charset = 'UTF-8';

				       	$mail -> isMail();

				       	$mail -> setFrom('cursos@tutorialesatualcance.com', 'Tutoriales a tu alcance');

				       	$mail -> addReplyTo('cursos@tutorialesatualcance.com', 'Tutoriales a tu alcance');

				       	$mail ->Subject = "Por favor verifique su direccion de correo electronico";

				       	$mail -> addAddress($_POST["regEmail"]);

						$mail -> msgHTML('<div style="width:100%; background:#eee; position:relative; font-family: sans-serif; padding-bottom: 40px;">
								
											<center>

													<img style="padding: 20px; width: 10%;" src="https://tutorialesatualcance.com/tienda/logo.png">
												
											</center>

											<div style="position: relative; margin: auto; width: 600px; background: white; padding: 20px;">

												<center>

													<img style="padding: 20px; width: 15%;" src="https://tutorialesatualcance.com/tienda/icon-email.png">

													<h3 style="font-weight: 100; color: #999;">Verifique su direccion de correo electronico</h3>

													<hr style="border:1px solid #ccc; width:80%;">

													<h4 style="font-weight: 100; color: #999; padding: 0 20px;">Para comenzar a usar su cuenta de tienda virtual, debe confirmar su direccion de correo electronico</h4>
													
													<a href="'.$url.'verificar/'.$encriptarEmail.'" target="_blank" style="text-decoration: none;">
														<div style="line-height: 60px; background: #0aa; width: 60%; color: white;">Verifique su direccion de correo electronico</div>
													</a>

													<br>

													<hr style="border:1px solid #ccc; width:80%;">

													<h5 style="font-weight: 100; color: #999;">Si no se inscribió en esta pagina, puede ignorar este correo electronico y la cuenta se eliminará</h5>

												</center>
											
											</div>

										</div>');

						$exito = $mail -> Send();

						if(!$exito){

						echo '<script> 

							swal({
								
									title: "Lo sentimos!",
									text: "Ha ocurrido un problema enviado verificacion de correo electronico a '.$_POST["regEmail"].$mail->ErrorInfo.'!",
									type: "error",
									confirmButtonText: "Cerrar",
									closeOnConfirm: false
								},
									function(isConfirm){
										
										if(isConfirm){
											history.back();
										}

								});

	      				</script>';

						}else{

						echo '<script> 

							swal({
								
									title: "Muy bien!!!",
									text: "Ahora revise su correo electronico '.$_POST["regEmail"].' en bandeja de entrada o spam para verificar la cuenta",
									type: "success",
									confirmButtonText: "Cerrar",
									closeOnConfirm: false
								},
									function(isConfirm){
										
										if(isConfirm){
											history.back();
										}

								});

	      				</script>';


						}
			     
			       }

      			}else{

      				echo '<script> 

						swal({
							
								title: "Error!",
								text: "Error al registrar el usuario, no se permiten caracteres especiales",
								type: "error",
								confirmButtonText: "Cerrar",
								closeOnConfirm: false
							},
								function(isConfirm){
									
									if(isConfirm){
										history.back();
									}

							});

      				</script>';

      			}

      		}

      	}

      	/*=============================================
		=         MOSTRAR USUARIO          =
		=============================================*/

		static public function ctrMostrarUsuario($item, $valor){

			$tabla = "usuarios";

			$respuesta = ModeloUsuarios::mdlMostrarUsuarios($tabla, $item, $valor);

			return $respuesta;

		}

		/*=============================================
		=       ACTUALIZAR USUARIO       =
		=============================================*/

		static public function ctrActualizarUsuario($id, $item2, $valor2){

			$tabla = "usuarios";

			$respuesta = ModeloUsuarios::mdlActualizarUsuario($tabla, $id, $item2, $valor2);

			return $respuesta;

		}

		/*=============================================
		=       INGRESO USUARIO       =
		=============================================*/
		
		static public function ctrIngresoUsuario(){

			if(isset($_POST["ingEmail"])){

				if(preg_match('/^[^0-9][a-zA-Z0-9_]+([.][a-zA-Z0-9_]+)*[@][a-zA-Z0-9_]+([.][a-zA-Z0-9_]+)*[.][a-zA-Z]{2,4}$/', $_POST["ingEmail"]) &&
				   preg_match('/^[a-zA-Z0-9]+$/', $_POST["ingPassword"])){

					$encriptar = crypt($_POST["ingPassword"], '$2a$07$asxx54ahjppf45sd87a5a4dDDGsystemdev$');

					$tabla = "usuarios";
					$item = "email";
					$valor = $_POST["ingEmail"];

					$respuesta = ModeloUsuarios::mdlMostrarUsuarios($tabla, $item, $valor);

					if($respuesta["email"] == $_POST["ingEmail"] && $respuesta["password"] == $encriptar){

						if($respuesta["verificacion"] == 1){

							
						echo '<script> 

							swal({
								
									title: "Aun no ha verificado su correo electronico!",
									text: "Por favor revise su bandeja de entrada o la carpeta de SPAM de su correo para verificar la direccion de correo elenctronico '.$respuesta["email"].'!",
									type: "error",
									confirmButtonText: "Cerrar",
									closeOnConfirm: false
								},
									function(isConfirm){
										
										if(isConfirm){
											history.back();
										}

								});

	      				</script>';

						}else{

							$_SESSION["validarSesion"] = "ok";
							$_SESSION["id"] = $respuesta["id"];
							$_SESSION["nombre"] = $respuesta["nombre"];
							$_SESSION["foto"] = $respuesta["foto"];
							$_SESSION["email"] = $respuesta["email"];
							$_SESSION["password"] = $respuesta["password"];
							$_SESSION["modo"] = $respuesta["modo"];

							echo '<script>

									window.location = localStorage.getItem("rutaActual");

								</script>';

						}

					}else{

						echo '<script> 

							swal({
								
									title: "Error al ingresar!",
									text: "Por favor revisar que el usuario exista o el password coincida con la registrada!",
									type: "error",
									confirmButtonText: "Cerrar",
									closeOnConfirm: false
								},
									function(isConfirm){
										
										if(isConfirm){
											window.location = localStorage.getItem("rutaActual");
										}

								});

	      				</script>';

					}

				}else{

					echo '<script> 

						swal({
							
								title: "Error!",
								text: "Error al ingresar, no se permiten caracteres especiales",
								type: "error",
								confirmButtonText: "Cerrar",
								closeOnConfirm: false
							},
								function(isConfirm){
									
									if(isConfirm){
										window.location = localStorage.getItem("rutaActual");
									}

							});

      				</script>';

				}

			}

		}

		/*=============================================
		=          OLVIDO PASSWORD          =
		=============================================*/

		public function ctrOlvidoPassword(){

			if(isset($_POST["passEmail"])){

				if(preg_match('/^[^0-9][a-zA-Z0-9_]+([.][a-zA-Z0-9_]+)*[@][a-zA-Z0-9_]+([.][a-zA-Z0-9_]+)*[.][a-zA-Z]{2,4}$/', $_POST["passEmail"])){

						/*=============================================
						=          GENERAR PASSWORD ALEATORIO         =
						=============================================*/

						function generarPassword($longitud){

							$key = "";
							$pattern = "1234567890abcdefghijklmnopqrstuvwxyz";

							$max = strlen($pattern)-1;

							for ($i=0; $i < $longitud; $i++) { 
								
								$key .= $pattern{mt_rand(0, $max)};

							}

							return $key;

						}

							$nuevoPassword = generarPassword(11);

							$encriptar = crypt($nuevoPassword, '$2a$07$asxx54ahjppf45sd87a5a4dDDGsystemdev$');

							$tabla = "usuarios";

							$item1 = "email";
							$valor1 = $_POST["passEmail"];

							$respuesta1 = ModeloUsuarios::mdlMostrarUsuarios($tabla, $item1, $valor1);

						if($respuesta1){

							$id = $respuesta1["id"];
							$item2 = "password";
							$valor2 = $encriptar;

							$respuesta2 = ModeloUsuarios::mdlActualizarUsuario($tabla, $id, $item2, $valor2);

							if($respuesta2 == "ok"){

							       	/*=============================================
							       	=    CAMBIO DE PASSWORD     =
							       	=============================================*/

							       	date_default_timezone_get("America/Mexico_City");

							       	$url = Ruta::ctrRuta();

							       	$mail = new PHPMailer;

							       	$mail ->Charset = 'UTF-8';

							       	$mail -> isMail();

							       	$mail -> setFrom('cursos@tutorialesatualcance.com', 'Tutoriales a tu alcance');

							       	$mail -> addReplyTo('cursos@tutorialesatualcance.com', 'Tutoriales a tu alcance');

							       	$mail ->Subject = "Solicitud de nueva Contraseña";

							       	$mail -> addAddress($_POST["passEmail"]);

									$mail -> msgHTML('<div style="width:100%; background:#eee; position:relative; font-family: sans-serif; padding-bottom: 40px;">
			
											<center>

													<img style="padding: 20px; width: 10%;" src="https://tutorialesatualcance.com/tienda/logo.png">
												
											</center>

											<div style="position: relative; margin: auto; width: 600px; background: white; padding: 20px;">

												<center>

													<img style="padding: 20px; width: 15%;" src="https://tutorialesatualcance.com/tienda/icon-pass.png">

													<h3 style="font-weight: 100; color: #999;">Solicitud de nueva Contraseña</h3>

													<hr style="border:1px solid #ccc; width:80%;">

													<h4 style="font-weight: 100; color: #999; padding: 0 20px;">Su Nueva Contraseña: <strong>'.$nuevoPassword.'</strong></h4>
													
													<a href="'.$url.'" target="_blank" style="text-decoration: none;">
														<div style="line-height: 60px; background: #0aa; width: 60%; color: white;">Ingrese nuevamente al sitio</div>
													</a>

													<br>

													<hr style="border:1px solid #ccc; width:80%;">

													<h5 style="font-weight: 100; color: #999;">Si no se inscribió en esta pagina, puede ignorar este correo electronico y la cuenta se eliminará</h5>

												</center>
											
											</div>

										</div>');

									$exito = $mail -> Send();

									if(!$exito){

									echo '<script> 

										swal({
											
												title: "Lo sentimos!",
												text: "Ha ocurrido un problema enviando cambio de Contraseña a '.$_POST["passEmail"].$mail->ErrorInfo.'!",
												type: "error",
												confirmButtonText: "Cerrar",
												closeOnConfirm: false
											},
												function(isConfirm){
													
													if(isConfirm){
														history.back();
													}

											});

				      				</script>';

									}else{

									echo '<script> 

										swal({
											
												title: "Muy bien!!!",
												text: "Por favor revise su correo electronico '.$_POST["passEmail"].' en bandeja de entrada o SPAM para su cambio de Contraseña",
												type: "success",
												confirmButtonText: "Cerrar",
												closeOnConfirm: false
											},
												function(isConfirm){
													
													if(isConfirm){
														history.back();
													}

											});

				      				</script>';


									}
						     
						       }

						}else{

							echo '<script> 

							swal({
								
									title: "Error!",
									text: "Error, el correo electronico no existe en el sistema!",
									type: "error",
									confirmButtonText: "Cerrar",
									closeOnConfirm: false
								},
									function(isConfirm){
										
										if(isConfirm){
											window.location = localStorage.getItem("rutaActual");
										}

								});

	      				</script>';

						}

				}else{

				echo '<script> 

						swal({
							
								title: "Error!",
								text: "Error al enviar el correo, puede estar mal escrito o contener caracteres especiales no permitidos",
								type: "error",
								confirmButtonText: "Cerrar",
								closeOnConfirm: false
							},
								function(isConfirm){
									
									if(isConfirm){
										window.location = localStorage.getItem("rutaActual");
									}

							});

	  				</script>';

				}

			}

		}

	/*=============================================
	REGISTRO CON REDES SOCIALES
	=============================================*/

	static public function ctrRegistroRedesSociales($datos){

		$tabla = "usuarios";
		$item = "email";
		$valor = $datos["email"];
		$emailRepetido = false;

		$respuesta0 = ModeloUsuarios::mdlMostrarUsuarios($tabla, $item, $valor);

		if($respuesta0){

			if($respuesta0["modo"] != $datos["modo"]){

				echo '<script> 

						swal({
							
								title: "Error!",
								text: "El correo electronico '.$datos["email"].' ya esta registrado en el sistema con un metodo distinto a Google",
								type: "error",
								confirmButtonText: "Cerrar",
								closeOnConfirm: false
							},
								function(isConfirm){
									
									if(isConfirm){
										history.back();
									}

							});

	  				</script>';

	  				$emailRepetido = false;

	  				return;

			}

			$emailRepetido = true;

		}else{

			$respuesta1 = ModeloUsuarios::mdlRegistroUsuario($tabla, $datos);

		}

		if($emailRepetido || $respuesta1 == "ok"){

			$respuesta2 = ModeloUsuarios::mdlMostrarUsuarios($tabla, $item, $valor);

			if($respuesta2["modo"] == "facebook"){

				session_start();

				$_SESSION["validarSesion"] = "ok";
				$_SESSION["id"] = $respuesta2["id"];
				$_SESSION["nombre"] = $respuesta2["nombre"];
				$_SESSION["foto"] = $respuesta2["foto"];
				$_SESSION["email"] = $respuesta2["email"];
				$_SESSION["password"] = $respuesta2["password"];
				$_SESSION["modo"] = $respuesta2["modo"];

				echo "ok";

			}else if($respuesta2["modo"] == "google"){

				$_SESSION["validarSesion"] = "ok";
				$_SESSION["id"] = $respuesta2["id"];
				$_SESSION["nombre"] = $respuesta2["nombre"];
				$_SESSION["foto"] = $respuesta2["foto"];
				$_SESSION["email"] = $respuesta2["email"];
				$_SESSION["password"] = $respuesta2["password"];
				$_SESSION["modo"] = $respuesta2["modo"];

				echo "ok";

			}else{

				echo "";
			}

		}
	}

	/*=============================================
		=       Actualizar Perfil       =
	=============================================*/

	public function ctrActualizarPerfil(){

		if(isset($_POST["editarNombre"])){

			/* VALIDAR IMAGEN */

			$ruta = $_POST["fotoUsuario"];

			if(isset($_FILES["datosImagen"]["tmp_name"]) && !empty($_FILES["datosImagen"]["tmp_name"])){

				/* PREGUNTAMOS SI EXISTE OTRA IMAGEN EN LA BASE DE DATOS */

				$directorio = "vistas/img/usuarios/".$_POST["idUsuario"];

				if(!empty($_POST["fotoUsuario"])){

					unlink($_POST["fotoUsuario"]);

				}else{

					mkdir($directorio, 0755);

				}

				/* GUARDAMOS LA IMAGEN EN EL DIRECTORIO */

				list($ancho, $alto) = getimagesize($_FILES["datosImagen"]["tmp_name"]);

				$nuevoAncho = 500;
				$nuevoAlto = 500;

				$aleatoria = mt_rand(100, 999);

				if($_FILES["datosImagen"]["type"] == "image/jpeg"){

					$ruta = "vistas/img/usuarios/".$_POST["idUsuario"]."/".$aleatoria.".jpg";

					/* MODIFICAMOS EL TAMANO DE LA FOTO */

					$origen = imagecreatefromjpeg($_FILES["datosImagen"]["tmp_name"]);

					$destino = imagecreatetruecolor($nuevoAncho, $nuevoAlto);

					imagecopyresized($destino, $origen, 0, 0, 0, 0, $nuevoAncho, $nuevoAlto, $ancho, $alto);

					imagejpeg($destino, $ruta);	

				}else if($_FILES["datosImagen"]["type"] == "image/png"){

						$ruta = "vistas/img/usuarios/".$_POST["idUsuario"]."/".$aleatoria.".png";

						/* MODIFICAMOS EL TAMANO DE LA FOTO */

						$origen = imagecreatefrompng($_FILES["datosImagen"]["tmp_name"]);

						$destino = imagecreatetruecolor($nuevoAncho, $nuevoAlto);

						imagealphablending($destino, FALSE);

						imagesavealpha($destino, TRUE);

						imagecopyresized($destino, $origen, 0, 0, 0, 0, $nuevoAncho, $nuevoAlto, $ancho, $alto);

						imagejpeg($destino, $ruta);	

				}

			}
			

			if($_POST["editarPassword"] == ""){

				$password = $_POST["passUsuario"];

			}else{

				$password = crypt($_POST["editarPassword"], '$2a$07$asxx54ahjppf45sd87a5a4dDDGsystemdev$');

			}

			$tabla = "usuarios";

			$datos = array("nombre" => $_POST["editarNombre"],
						   "email" => $_POST["editarEmail"],
						   "password" => $password,
						   "foto" => $ruta,
						   "id" => $_POST["idUsuario"],
						   "modo" => $_POST["modoUsuario"],);

			$respuesta = ModeloUsuarios::mdlActualizarPerfil($tabla, $datos);

			if($respuesta == "ok"){

				$_SESSION["validarSesion"] = "ok";
				$_SESSION["id"] = $datos["id"];
				$_SESSION["nombre"] = $datos["nombre"];
				$_SESSION["foto"] = $datos["foto"];
				$_SESSION["email"] = $datos["email"];
				$_SESSION["password"] = $datos["password"];
				$_SESSION["modo"] = $datos["modo"];

				echo '<script> 

						swal({
							
								title: "Ok!",
								text: "Su cuenta ha sido actualizada correctamente",
								type: "success",
								confirmButtonText: "Cerrar",
								closeOnConfirm: false
							},
								function(isConfirm){
									
									if(isConfirm){
										history.back();
									}

							});

	  				</script>';
			}

		}

	}

	/*=============================================
		=       Actualizar Perfil       =
	=============================================*/

	static public function ctrMostrarCompras($item, $valor){

		$tabla = "compras";

		$respuesta = ModeloUsuarios::mdlMostrarCompras($tabla, $item, $valor);

		return $respuesta;

	}

	/*=============================================
		=       Comentarios Perfil       =
	=============================================*/

	static public function ctrMostrarComentariosPerfil($datos){

		$tabla = "comentarios";

		$respuesta = ModeloUsuarios::mdlMostrarComentariosPerfil($tabla, $datos);

		return $respuesta;

	}

	/*=============================================
		=       Actualizar Comentarios       =
	=============================================*/

	public function ctrActualizarComentario(){

		if(isset($_POST["idComentario"])){

			if(preg_match('/^[,\\.\\a-zA-ZñÑáéíóúÁÉÍÓÚ ]+$/', $_POST["comentario"])){

				if($_POST["comentario"] != ""){

					$tabla = "comentarios";

					$datos = array("id"=>$_POST["idComentario"],
								   "calificacion"=>$_POST["puntaje"],
								   "comentario"=>$_POST["comentario"]);

					$respuesta = ModeloUsuarios::mdlActualizarComentario($tabla, $datos);

					if($respuesta == "ok"){

						echo '<script> 

						swal({
							
								title: "Gracias por su opinion!",
								text: "Su calificacion y comentario ha sido guardado!",
								type: "success",
								confirmButtonText: "Cerrar",
								closeOnConfirm: false
							},
								function(isConfirm){
									
									if(isConfirm){
										history.back();
									}

							});

	  				</script>';

					}

				}else{

					echo '<script> 

						swal({
							
								title: "Error al enviar su calificacion!",
								text: "El comentario no puede estar vacio",
								type: "error",
								confirmButtonText: "Cerrar",
								closeOnConfirm: false
							},
								function(isConfirm){
									
									if(isConfirm){
										history.back();
									}

							});

	  				</script>';

				}

			}else{

				echo '<script> 

						swal({
							
								title: "Error al enviar comentario!",
								text: "El comentario no puede llevar caracteres especiales",
								type: "error",
								confirmButtonText: "Cerrar",
								closeOnConfirm: false
							},
								function(isConfirm){
									
									if(isConfirm){
										history.back();
									}

							});

	  				</script>';

			}

		}

	}

	/*=============================================
		=       Agregar a lista de deseos       =
	=============================================*/

	static public function ctrAgregarDeseo($datos){

		$tabla = "deseos";

		$respuesta = ModeloUsuarios::mdlAgregarDeseo($tabla, $datos);

		return $respuesta;

	}

	/*=============================================
		=       Mostrar lista de deseos       =
	=============================================*/

	static public function ctrMostrarListaDeseos($item){

		$tabla = "deseos";

		$respuesta = ModeloUsuarios::mdlMostrarDeseos($tabla, $item);

		return $respuesta;

	}

	/*=============================================
		=       Quitar lista de deseos       =
	=============================================*/

	static public function ctrQuitarDeseo($datos){

		$tabla = "deseos";

		$respuesta = ModeloUsuarios::mdlQuitarDeseo($tabla, $datos);

		return $respuesta;

	}

	/*=============================================
		=       Eliminar Usuario       =
	=============================================*/

	public function ctrEliminarUsuario(){

			if(isset($_GET["id"])){

				$tabla1 = "usuarios";
				$tabla2 = "comentarios";
				$tabla3 = "compras";
				$tabla4 = "deseos";

				$id = $_GET["id"];

				if($_GET["foto"] != ""){

						unlink($_GET["foto"]);
						rmdir("vistas/img/usuarios/".$_GET["id"]);
				}

				$respuesta = ModeloUsuarios::mdlEliminarUsuario($tabla1, $id);

				ModeloUsuarios::mdlEliminarComentarios($tabla2, $id);
				ModeloUsuarios::mdlEliminarCompras($tabla3, $id);
				ModeloUsuarios::mdlEliminarListaDeseos($tabla4, $id);

				if($respuesta == "ok"){

						echo '<script> 

						swal({
							
								title: "Su cuenta ha sido borrada!",
								text: "Debe registrarse nuevamente si desea ingresar",
								type: "success",
								confirmButtonText: "Cerrar",
								closeOnConfirm: false
							},
								function(isConfirm){
									
									if(isConfirm){
										window.location = "'.$url.'salir";
									}

							});

	  				</script>';

				}

			}

	}

	/*==============================================
	=            Formulario Contactenos            =
	==============================================*/

	public function ctrFormularioContactenos(){

		if(isset($_POST["mensajeContactenos"])){

			if(preg_match('/^[a-zA-ZñÑáéíóúÁÉÍÓÚ ]+$/', $_POST["nombreContactenos"]) &&
				   preg_match('/^[^0-9][a-zA-Z0-9_]+([.][a-zA-Z0-9_]+)*[@][a-zA-Z0-9_]+([.][a-zA-Z0-9_]+)*[.][a-zA-Z]{2,4}$/', $_POST["emailContactenos"]) &&
				   preg_match('/^[,\\.\\a-zA-ZñÑáéíóúÁÉÍÓÚ ]+$/', $_POST["mensajeContactenos"])){

				/*================================================
				=            Envio Correo Electronico            =
				================================================*/

				date_default_timezone_get("America/Mexico_City");

			       	$url = Ruta::ctrRuta();

			       	$mail = new PHPMailer;

			       	$mail ->Charset = 'UTF-8';

			       	$mail -> isMail();

			       	$mail -> setFrom('cursos@tutorialesatualcance.com', 'Tutoriales a tu alcance');

			       	$mail -> addReplyTo('cursos@tutorialesatualcance.com', 'Tutoriales a tu alcance');

			       	$mail ->Subject = "Ha recibido una consulta";

			       	$mail -> addAddress("contacto@tiendaonline.com");

					$mail -> msgHTML('<div style="width:100%; background:#eee; position:relative; font-family:sans-serif; padding-bottom:40px">

						<center><img style="padding:20px; width:10%" src="http://www.tutorialesatualcance.com/tienda/logo.png"></center>

						<div style="position:relative; margin:auto; width:600px; background:white; padding-bottom:20px">

							<center>

							<img style="padding-top:20px; width:15%" src="http://www.tutorialesatualcance.com/tienda/icon-email.png">


							<h3 style="font-weight:100; color:#999;">HA RECIBIDO UNA CONSULTA</h3>

							<hr style="width:80%; border:1px solid #ccc">

							<h4 style="font-weight:100; color:#999; padding:0px 20px; text-transform:uppercase">'.$_POST["nombreContactenos"].'</h4>

							<h4 style="font-weight:100; color:#999; padding:0px 20px;">De: '.$_POST["emailContactenos"].'</h4>

							<h4 style="font-weight:100; color:#999; padding:0px 20px">'.$_POST["mensajeContactenos"].'</h4>

							<hr style="width:80%; border:1px solid #ccc">

							</center>

						</div>

					</div>');

					$exito = $mail -> Send();

					if(!$exito){

					echo '<script> 

						swal({
							
								title: "Lo sentimos!",
								text: "Ha ocurrido un problema enviando el mensaje",
								type: "error",
								confirmButtonText: "Cerrar",
								closeOnConfirm: false
							},
								function(isConfirm){
									
									if(isConfirm){
										history.back();
									}

							});

      				</script>';

					}else{

					echo '<script> 

						swal({
							
								title: "Recibido!!",
								text: "Su mensaje ha sido enviado, muy pronto le responderemos",
								type: "success",
								confirmButtonText: "Cerrar",
								closeOnConfirm: false
							},
								function(isConfirm){
									
									if(isConfirm){
										history.back();
									}

							});

      				</script>';


					}
		     
			}else{

				echo '<script> 

						swal({
							
								title: "Error!",
								text: "Error al enviar el mensaje, puede estar mal escrito o contener caracteres especiales no permitidos",
								type: "error",
								confirmButtonText: "Cerrar",
								closeOnConfirm: false
							},
								function(isConfirm){
									
									if(isConfirm){
										history.back();
									}

							});

	  				</script>';

			}
		}
	}
}