<?php 

	class ControladorVisitas{

		/*=================================
		=            GuardarIP            =
		=================================*/

		static public function ctrEnviarIP($ip, $pais, $codigo){

			$tabla = "visitaspersonas";
			$visita = 1; 

			$respuestaInsertarIp = null;
			$respuestaActualizarIp = null;

			if($pais == ""){

				$pais = "Unknown";

			}

			/*===========================================
			=            Buscar IP Existente            =
			===========================================*/

			$buscarIpExistente = ModeloVisitas::mdlSeleccionarIP($tabla, $ip);

			if(!$buscarIpExistente){

				/*=============================================
				=            Guardamos la IP nueva            =
				=============================================*/

				$respuestaInsertarIp = ModeloVisitas::mdlGuardarNuevaIP($tabla, $ip, $pais, $visita);

			}else{

				/*============================================================================
				=            Si la IP existe y es otro dia, la volvemos a guardar            =
				============================================================================*/

				date_default_timezone_set("America/Mexico_City");

			    $fechaActual = date('Y-m-d');
		        $encontroFecha = false;
		      
		        foreach($buscarIpExistente as $key => $value)
		        {
		          $compararFecha = substr($value["fecha"], 0, 10);
		        
		          if($fechaActual == $compararFecha)
		          {
		            $encontroFecha = true;
		          }
		        
		        }
		      
		        if(!$encontroFecha)
		        {
		          $respuestaActualizarIp = ModeloVisitas::mdlGuardarNuevaIP($tabla, $ip, $pais, $visita);
		        }

			}

			if($respuestaActualizarIp == "ok" || $respuestaInsertarIp == "ok"){

				$tablaPais = "visitapaises";

				/*========================================
				=            Seleccionar Pais            =
				========================================*/
				
				$seleccionarPais = ModeloVisitas::mdlSeleccionarPais($tablaPais, $pais);

				if(!$seleccionarPais){

					$cantidad = 1;

					$insertarPais = ModeloVisitas::mdlInsertarPais($tablaPais, $pais, $codigo, $cantidad);

				}else{

					/*=============================================================================
					=            Si el pais existe hay que actualizar una nueva visita            =
					=============================================================================*/

					$actualizarCantidad = $seleccionarPais["cantidad"] + 1;

					$actualizarPais = ModeloVisitas::mdlActualizarPais($tablaPais, $pais, $actualizarCantidad);
					

				}
				
			}

		}

		/*===================================================
		=            Mostrar el total de visitas            =
		===================================================*/

		public function ctrMostrarTotalVisitas(){

			$tabla = "visitapaises";

			$respuesta = ModeloVisitas::mdlMostrarTotalVisitas($tabla);

			return $respuesta;

		}

		/*=====================================================
		=            Mostrar los primeros 6 Paises            =
		=====================================================*/
		
		public function ctrMostrarPaises(){

			$tabla = "visitapaises";

			$respuesta = ModeloVisitas::mdlMostrarPaises($tabla);

			return $respuesta;

		}
		
	}