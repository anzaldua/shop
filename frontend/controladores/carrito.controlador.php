<?php

	class ControladorCarrito{

		/* MOSTRAR TARIFAS */

		public function ctrMostrarTarifas(){

			$tabla = "comercio";

			$respuesta = ModeloCarrito::mdlMostrarTarifas($tabla);

			return $respuesta;

		}

		/* Nuevas Compras */

		static public function ctrNuevasCompras($datos){

			$tabla = "compras";

			$respuesta = ModeloCarrito::mdlNuevasCompras($tabla, $datos);

			if($respuesta == "ok"){

				$tabla = "comentarios";

				ModeloUsuarios::mdlIngresoComentarios($tabla, $datos);

			}

			return $respuesta;

		}

	}