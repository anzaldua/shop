<?php 
	
	class ControladorProductos{

		/* MOSTRAR CATEGORIAS */

		static public function ctrMostrarCategorias($item, $valor){

			$tabla = "categorias";

			$respuesta = ModeloProductos::mdlMostrarCategorias($tabla, $item, $valor);

			return $respuesta;

		}

		/* MOSTRAR SUBCATEGORIAS */

		static public function ctrMostrarSubCategorias($item, $valor){

			$tabla = "subcategorias";

			$respuesta = ModeloProductos::mdlMostrarSubCategorias($tabla, $item, $valor);

			return $respuesta;

		}

		/* MOSTRAR PRODUCTOS */

		static public function ctrMostrarProductos($ordenar, $item, $valor, $base, $tope, $modo){

			$tabla = "productos";

			$respuesta = ModeloProductos::mdlMostrarProductos($tabla, $ordenar, $item, $valor, $base, $tope, $modo);

			return $respuesta;

		}

		/* MOSTRAR INFO-PRODUCTOS */

		static public function ctrMostrarInfoProducto($item, $valor){

			$tabla = "productos";

			$respuesta = ModeloProductos::mdlMostrarInfoProductos($tabla, $item, $valor);

			return $respuesta;

		}

		/* LISTAR PRODUCTOS */

		static public function ctrListarProductos($ordenar, $item, $valor){

			$tabla = "productos";

			$respuesta = ModeloProductos::mdlListarProductos($tabla, $ordenar, $item, $valor);

			return $respuesta;

		}

		/* MOSTRAR BANNER */

		static public function ctrMostrarBanner($ruta){

			$tabla = "banner";

			$respuesta = ModeloProductos::mdlMostrarBanner($tabla, $ruta);

			return $respuesta;

		}

		/* BUSCAR PRODUCTO POR BUSCADOR */

		static public function ctrBuscarProductos($busqueda, $ordenar, $base, $tope, $modo){

			$tabla = "productos";

			$respuesta = ModeloProductos::mdlBuscarProductos($tabla, $busqueda, $ordenar, $base, $tope, $modo);

			return $respuesta;

		}

		/* LISTAR PRODUCTO POR BUSCADOR */

		static public function ctrListarProductosBusqueda($busqueda){

			$tabla = "productos";

			$respuesta = ModeloProductos::mdlListarProductosBusqueda($tabla, $busqueda);

			return $respuesta;

		}

		/* ACTUALIZAR LA VISTA DEL PRODUCTO PROVIENE DE AJAX*/

		static public function ctrActualizarProducto($item1, $valor1, $item2, $valor2){

			$tabla = "productos";

			$respuesta = ModeloProductos::mdlActualizarProducto($tabla, $item1, $valor1, $item2, $valor2);

			return $respuesta;

		}

		/* Verificar Producto Gratuito */

		static public function ctrVerificarProducto($datos){

			$tabla = "compras";

			$respuesta = ModeloCarrito::mdlVerificarProducto($tabla, $datos);

			return $respuesta;
		}
		
		

	}
	