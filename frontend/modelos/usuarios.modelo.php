<?php
	
	require_once "conexion.php";


		class ModeloUsuarios{

			/*=============================================
			=         REGISTRO DE USUARIO          =
			=============================================*/
			

			static public function mdlRegistroUsuario($tabla, $datos){

				$stmt = Conexion::conectar()->prepare("INSERT INTO $tabla(nombre, password, email, foto, modo, verificacion, emailEncriptado) VALUES(:nombre, :password, :email, :foto, :modo, :verificacion, :emailEncriptado)");

				$stmt->bindParam(":nombre", $datos["nombre"], PDO::PARAM_STR);
				$stmt->bindParam(":password", $datos["password"], PDO::PARAM_STR);
				$stmt->bindParam(":email", $datos["email"], PDO::PARAM_STR);
				$stmt->bindParam(":foto", $datos["foto"], PDO::PARAM_STR);
				$stmt->bindParam(":modo", $datos["modo"], PDO::PARAM_STR);
				$stmt->bindParam(":verificacion", $datos["verificacion"], PDO::PARAM_INT);
				$stmt->bindParam(":emailEncriptado", $datos["emailEncriptado"], PDO::PARAM_STR);

				if($stmt->execute()){

					return "ok";

				}else{

					return "error";

				}

				$stmt ->close();

				$stmt = null; 


			}


			/*=============================================
			=         MOSTRAR USUARIO          =
			=============================================*/

			static public function mdlMostrarUsuarios($tabla, $item, $valor){

				$stmt = Conexion::conectar()->prepare("SELECT * from $tabla WHERE $item = :$item");

				$stmt -> bindParam(":".$item, $valor, PDO::PARAM_STR);

				$stmt ->execute();

				return $stmt -> fetch();

				$stmt ->close();

				$stmt = null;

			}

			/*=============================================
			=        ACTUALIZAR USUARIO        =
			=============================================*/

			static public function mdlActualizarUsuario($tabla, $id, $item2, $valor2){

				$stmt = Conexion::conectar()->prepare("UPDATE $tabla SET $item2 = :$item2 WHERE id = :id");

				$stmt -> bindParam(":".$item2, $valor2, PDO::PARAM_STR);
				$stmt -> bindParam(":id", $id,  PDO::PARAM_INT);

				if($stmt ->execute()){

					return "ok";

				}else{

					return "error";
				}

				$stmt ->close();

				$stmt = null;

			}

			/*=============================================
			=        Actualizar Perfil        =
			=============================================*/

			static public function mdlActualizarPerfil($tabla, $datos){

				$stmt = Conexion::conectar()->prepare("UPDATE $tabla SET nombre = :nombre, email = :email, password = :password, foto = :foto WHERE id = :id");

				$stmt -> bindParam(":nombre", $datos["nombre"], PDO::PARAM_STR);
				$stmt -> bindParam(":email", $datos["email"], PDO::PARAM_STR);
				$stmt -> bindParam(":password", $datos["password"], PDO::PARAM_STR);
				$stmt -> bindParam(":foto", $datos["foto"], PDO::PARAM_STR);
				$stmt -> bindParam(":id", $datos["id"], PDO::PARAM_INT);

				if($stmt ->execute()){

					return "ok";

				}else{

					return "error";
				}

				$stmt ->close();

				$stmt = null;

			}

			/*=============================================
			=         MOSTRAR COMPRAS          =
			=============================================*/

			static public function mdlMostrarCompras($tabla, $item, $valor){

				$stmt = Conexion::conectar()->prepare("SELECT * from $tabla WHERE $item = :$item");

				$stmt -> bindParam(":".$item, $valor, PDO::PARAM_STR);

				$stmt ->execute();

				return $stmt -> fetchAll();

				$stmt ->close();

				$stmt = null;

			}

			/*=============================================
			=         Mostrar Comentarios por perfil          =
			=============================================*/

			static public function mdlMostrarComentariosPerfil($tabla, $datos){

				if($datos["idUsuario"] != ""){

					$stmt = Conexion::conectar()->prepare("SELECT * from $tabla WHERE id_usuario = :id_usuario AND id_producto = :id_producto");

					$stmt -> bindParam(":id_usuario", $datos["idUsuario"], PDO::PARAM_STR);
					$stmt -> bindParam(":id_producto", $datos["idProducto"],PDO::PARAM_STR);

					$stmt ->execute();

					return $stmt -> fetch();

				}else{

					$stmt = Conexion::conectar()->prepare("SELECT * from $tabla WHERE id_producto = :id_producto ORDER BY Rand()");

					$stmt -> bindParam(":id_producto", $datos["idProducto"],PDO::PARAM_STR);

					$stmt ->execute();

					return $stmt -> fetchAll();

				}

				$stmt ->close();

				$stmt = null;

			}

			/*=============================================
			=        Actualizar Comentario        =
			=============================================*/

			static public function mdlActualizarComentario($tabla, $datos){

				$stmt = Conexion::conectar()->prepare("UPDATE $tabla SET calificacion = :calificacion, comentario = :comentario WHERE id = :id");

				$stmt -> bindParam(":calificacion", $datos["calificacion"], PDO::PARAM_STR);
				$stmt -> bindParam(":comentario", $datos["comentario"], PDO::PARAM_STR);
				$stmt -> bindParam(":id", $datos["id"], PDO::PARAM_INT);

				if($stmt ->execute()){

					return "ok";

				}else{

					return "error";
				}

				$stmt ->close();

				$stmt = null;

			}

			/*=============================================
			=         Agregar a lista de deseos          =
			=============================================*/

			static public function mdlAgregarDeseo($tabla, $datos){

				$stmt = Conexion::conectar()->prepare("INSERT INTO $tabla(id_usuario, id_producto) VALUES(:id_usuario, :id_producto)");

				$stmt -> bindParam(":id_usuario", $datos["idUsuario"], PDO::PARAM_INT);
				$stmt -> bindParam(":id_producto", $datos["idProducto"], PDO::PARAM_INT);

				if($stmt ->execute()){

					return "ok";

				}else{

					return "error";
				}

				$stmt ->close();

				$stmt = null;

			}

			/*=============================================
			=         Mostrar Deseos          =
			=============================================*/

			static public function mdlMostrarDeseos($tabla, $item){

				$stmt = Conexion::conectar()->prepare("SELECT * from $tabla WHERE id_usuario = :id_usuario order by id DESC");

				$stmt -> bindParam(":id_usuario", $item, PDO::PARAM_STR);

				$stmt ->execute();

				return $stmt -> fetchAll();

				$stmt ->close();

				$stmt = null;

			}	

			/*=============================================
			=         Quitar Deseos          =
			=============================================*/

			static public function mdlQuitarDeseo($tabla, $datos){

				$stmt = Conexion::conectar()->prepare("DELETE FROM $tabla WHERE id_producto = :id");

				$stmt -> bindParam(":id", $datos, PDO::PARAM_INT);

				if($stmt ->execute()){

					return "ok";

				}else{

					return "error";
				}

				$stmt ->close();

				$stmt = null;

			}	

			/*=============================================
			=         Eliminar Usuario          =
			=============================================*/

			static public function mdlEliminarUsuario($tabla, $id){

				$stmt = Conexion::conectar()->prepare("DELETE FROM $tabla WHERE id = :id");

				$stmt -> bindParam(":id", $id, PDO::PARAM_INT);

				if($stmt ->execute()){

					return "ok";

				}else{

					return "error";
				}

				$stmt ->close();

				$stmt = null;


			}

			/*=============================================
			=         Eliminar Comentarios de usuario  =
			=============================================*/

			static public function mdlEliminarComentarios($tabla, $id){

				$stmt = Conexion::conectar()->prepare("DELETE FROM $tabla WHERE id_usuario = :id_usuario");

				$stmt -> bindParam(":id_usuario", $id, PDO::PARAM_INT);

				if($stmt ->execute()){

					return "ok";

				}else{

					return "error";
				}

				$stmt ->close();

				$stmt = null;


			}

			/*=============================================
			=         Eliminar Compras de usuario          =
			=============================================*/

			static public function mdlEliminarCompras($tabla, $id){

				$stmt = Conexion::conectar()->prepare("DELETE FROM $tabla WHERE id_usuario = :id_usuario");

				$stmt -> bindParam(":id_usuario", $id, PDO::PARAM_INT);

				if($stmt ->execute()){

					return "ok";

				}else{

					return "error";
				}

				$stmt ->close();

				$stmt = null;


			}

			/*=============================================
			=         Eliminar Lista de deseo          =
			=============================================*/

			static public function mdlEliminarListaDeseos($tabla, $id){

				$stmt = Conexion::conectar()->prepare("DELETE FROM $tabla WHERE id_usuario = :id_usuario");

				$stmt -> bindParam(":id_usuario", $id, PDO::PARAM_INT);

				if($stmt ->execute()){

					return "ok";

				}else{

					return "error";
				}

				$stmt ->close();

				$stmt = null;


			}

			/*===========================================
			=            Ingreso Comentarios            =
			===========================================*/

			static public function mdlIngresoComentarios($tabla, $datos){

				$stmt = Conexion::conectar()->prepare("INSERT INTO $tabla(id_usuario, id_producto) VALUES(:id_usuario, :id_producto)");

				$stmt->bindParam("id_usuario", $datos["idUsuario"], PDO::PARAM_INT);
				$stmt->bindParam("id_producto", $datos["idProducto"], PDO::PARAM_INT);

				if($stmt->execute()){

					return "ok";

				}else{

					return "error";
				}

				$stmt->close();

			}
			

			
		}