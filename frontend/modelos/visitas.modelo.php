<?php 

	require_once "conexion.php";

	class ModeloVisitas{

		static public function mdlSeleccionarIP($tabla, $ip){

			$stmt = Conexion::conectar()->prepare("SELECT * FROM $tabla WHERE ip = :ip");

			$stmt->bindParam(":ip", $ip, PDO::PARAM_STR);

			$stmt->execute();

			return $stmt -> fetchAll();

			$stmt->close();

			$stmt = null;

		}

		static public function mdlGuardarNuevaIP($tabla, $ip, $pais, $visita){

			$stmt = Conexion::conectar()->prepare("INSERT INTO $tabla(ip, pais, visitas) VALUES(:ip, :pais, :visitas)");

			$stmt->bindParam(":ip", $ip, PDO::PARAM_STR);
			$stmt->bindParam(":pais", $pais, PDO::PARAM_STR);
			$stmt->bindParam(":visitas", $visita, PDO::PARAM_INT);

			if($stmt->execute()){

				return "ok";

			}else{

				return "error";

			}

			$stmt->close();

			$stmt = null;

		}

		/*=======================================================
		=            Insertar Pais en Visitas Paises            =
		=======================================================*/

		static public function mdlInsertarPais($tabla, $pais, $codigo, $cantidad){

			$stmt = Conexion::conectar()->prepare("INSERT INTO $tabla(pais, codigo, cantidad) VALUES (:pais, :codigo, :cantidad)");

			$stmt->bindParam(":pais", $pais, PDO::PARAM_STR);
			$stmt->bindParam(":codigo", $codigo, PDO::PARAM_STR);
			$stmt->bindParam(":cantidad", $cantidad, PDO::PARAM_INT);


			if($stmt->execute()){

				return "ok";

			}else{

				return "error";

			}

			$stmt->close();

			$stmt = null;

		}

		/*==============================================================================
		=            Revisar si ya existe el pais en la tabla visitaspaises            =
		==============================================================================*/
		

		static public function mdlSeleccionarPais($tabla, $pais){

			$stmt = Conexion::conectar()->prepare("SELECT * FROM $tabla WHERE pais = :pais");

			$stmt->bindParam(":pais", $pais, PDO::PARAM_STR);

			$stmt->execute();

			return $stmt -> fetch();

			$stmt->close();

			$stmt = null;

		}

		/*=====================================================================
		=            Si existe el pais actualizar una nueva visita            =
		=====================================================================*/

		static public function mdlActualizarPais($tabla, $pais, $actualizarCantidad){

			$stmt = Conexion::conectar()->prepare("UPDATE $tabla SET cantidad = :cantidad WHERE pais = :pais");

			$stmt->bindParam(":pais", $pais, PDO::PARAM_STR);
			$stmt->bindParam(":cantidad", $actualizarCantidad, PDO::PARAM_INT);

			if($stmt->execute()){

				return "ok";

			}else{

				return "error";

			}

			$stmt->close();

			$stmt = null;

		}

		/*=======================================================
		=            Mostrar el Total de las Visitas            =
		=======================================================*/

		static public function mdlMostrarTotalVisitas($tabla){

			$stmt = Conexion::conectar()->prepare("SELECT SUM(cantidad) as total FROM $tabla");

			$stmt ->execute();

			return $stmt -> fetch();

			$stmt->close();

			$stmt = null;

		}

		/*=====================================================
		=            Mostrar los primeros 6 Paises            =
		=====================================================*/
		

		static public function mdlMostrarPaises($tabla){

			$stmt = Conexion::conectar()->prepare("SELECT * FROM $tabla ORDER BY cantidad DESC LIMIT 6");

			$stmt ->execute();

			return $stmt -> fetchAll();

			$stmt->close();

			$stmt = null;

		}
		
	}