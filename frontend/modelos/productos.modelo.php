<?php 

	require_once "conexion.php";

	class ModeloProductos{

		/* MOSTRAR CATEGORIAS */

		static public function mdlMostrarCategorias($tabla, $item, $valor){

			if($item != null){

				$stmt = Conexion::conectar()->prepare("SELECT * FROM $tabla WHERE $item = :$item");

				$stmt -> bindParam(":".$item, $valor, PDO::PARAM_STR);

				$stmt -> execute();

				return $stmt -> fetch();

			}

				else{

				$stmt = Conexion::conectar()->prepare("SELECT * FROM $tabla");

				$stmt -> execute();

				return $stmt -> fetchAll();

			}

			$stmt -> close();

			$stmt = null;

		}

		/* MOSTRAR SUBCATEGORIAS */

		static public function mdlMostrarSubCategorias($tabla, $item, $valor){

		if($item != null){

			$stmt = Conexion::conectar()->prepare("SELECT * FROM $tabla WHERE $item = :$item");

			$stmt -> bindParam(":".$item, $valor, PDO::PARAM_STR);

			$stmt -> execute();

			return $stmt -> fetchAll();

		}else{

			$stmt = Conexion::conectar()->prepare("SELECT * FROM $tabla");

			$stmt -> execute();

			return $stmt -> fetchAll();

		}

			$stmt -> close();

			$stmt = null;

		}

		/* MOSTRAR PRODUCTOS */

		static public function mdlMostrarProductos($tabla, $ordenar, $item, $valor, $base, $tope, $modo){

			if($item != null){

				$stmt = Conexion::conectar()->prepare("SELECT * FROM $tabla WHERE $item = :$item ORDER BY $ordenar $modo limit $base, $tope");

				$stmt -> bindParam(":".$item, $valor, PDO::PARAM_STR);

				$stmt -> execute();

				return $stmt -> fetchAll();

			}else{

				$stmt = Conexion::conectar()->prepare("SELECT * FROM $tabla ORDER BY $ordenar $modo limit $base, $tope");

				$stmt -> execute();

				return $stmt -> fetchAll();

			}

			$stmt ->close();

			$stmt = null;

		}

		/* MOSTRAR INFO-PRODUCTO */

		static public function mdlMostrarInfoProductos($tabla, $item, $valor){

			$stmt = Conexion::conectar()->prepare("SELECT * FROM $tabla WHERE $item = :$item");

			$stmt -> bindParam(":".$item, $valor, PDO::PARAM_STR);

			$stmt -> execute();

			return $stmt -> fetch();

			$stmt -> close();

			$stmt = null;

		}

		/* LISTAR PRODUCTOS */

		static public function mdlListarProductos($tabla, $ordenar, $item, $valor){

			if($item != null){

				$stmt = Conexion::conectar()->prepare("SELECT * FROM $tabla WHERE $item = :$item ORDER BY $ordenar DESC");

				$stmt ->bindParam(":".$item, $valor, PDO::PARAM_STR);

				$stmt ->execute();

				return $stmt -> fetchAll();

			}else{

				$stmt = Conexion::conectar()->prepare("SELECT * FROM $tabla ORDER BY $ordenar DESC");

				$stmt ->execute();

				return $stmt -> fetchAll();

			}

			$stmt -> close();

			$stmt = null;

		}

		/* MOSTRAR BANNER */

		static public function mdlMostrarBanner($tabla, $ruta){

			$stmt = Conexion::conectar()->prepare("SELECT * FROM $tabla WHERE ruta = :ruta");

			$stmt -> bindParam(":ruta", $ruta, PDO::PARAM_STR);

			$stmt -> execute();

			return $stmt -> fetch();

			$stmt = close();

			$stmt = null;

		}

		/* BUSCAR PRODUCTOS MEDIANTE BUSCADOR */

		static public function mdlBuscarProductos($tabla, $busqueda, $ordenar, $base, $tope, $modo){

			$stmt = Conexion::conectar()->prepare("SELECT * FROM $tabla WHERE ruta like '%$busqueda%' OR titulo like '%$busqueda%' OR titular like '%$busqueda%' OR descripcion like '%$busqueda%' ORDER BY $ordenar $modo limit $base, $tope");

			$stmt -> execute();

			return $stmt -> fetchAll();

			$stmt = close();

			$stmt = null;

		}

		/* LISTAR PRODUCTOS BUSCADOR */

		static public function mdlListarProductosBusqueda($tabla, $busqueda){

			$stmt = Conexion::conectar()->prepare("SELECT * FROM $tabla WHERE ruta like '%$busqueda%' OR titulo like '%$busqueda%' OR titular like '%$busqueda%' OR descripcion like '%$busqueda%'");

			$stmt -> execute();

			return $stmt -> fetchAll();

			$stmt = close();

			$stmt = null;

		}

		/* ACTUALIZAR VISTA PRODUCTOS */

		static public function mdlActualizarProducto($tabla, $item1, $valor1, $item2, $valor2){

			$stmt = Conexion::conectar()->prepare("UPDATE $tabla SET $item1 = :$item1 WHERE $item2 = :$item2");

			$stmt -> bindParam(":".$item1, $valor1, PDO::PARAM_STR);
			$stmt -> bindParam(":".$item2, $valor2, PDO::PARAM_STR);

			if($stmt -> execute()){

				return "ok";

			}else{

				return "error";
			}

			$stmt = close();

			$stmt = null;

		}

	}