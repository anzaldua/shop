<?php

	require_once '../modelos/rutas.php';
	require_once "../modelos/carrito.modelo.php";

	use PayPal\Api\Amount;
	use PayPal\Api\Details;
	use PayPal\Api\Item;
	use PayPal\Api\ItemList;
	use PayPal\Api\Payer;
	use PayPal\Api\Payment;
	use PayPal\Api\RedirectUrls;
	use PayPal\Api\Transaction;

	class Paypal{

		static public function mdlPagoPaypal($datos){

			require __DIR__ . '/bootstrap.php';

			/*=============================================================================================
			=   Retiramos las , de los string que nos muestra para convertir en array            =
			=============================================================================================*/

			$tituloArray = explode(",", $datos["tituloArray"]);
			$cantidadArray = explode(",", $datos["cantidadArray"]);
			$valorItemArray = explode(",", $datos["valorItemArray"]);
			$idProductos = str_replace(",", "-", $datos["idProductoArray"]);
			$cantidadProductos = str_replace(",", "-", $datos["cantidadArray"]);
			$pagoProductos = str_replace(",", "-", $datos["valorItemArray"]);

			/*=======================================================================
			=            Seleccionamos que el metodo de pago sera paypal            =
			=======================================================================*/
			
			$payer = new Payer();
			$payer->setPaymentMethod("paypal");

			$item = array();
			$variosItems = array();

			for ($i=0; $i < count($tituloArray); $i++) { 
				
				$item[$i] = new Item();
				$item[$i]->setName($tituloArray[$i])
					     ->setCurrency($datos["divisa"])
					     ->setQuantity($cantidadArray[$i])
					     ->setPrice($valorItemArray[$i]/$cantidadArray[$i]);

					     array_push($variosItems, $item[$i]);
			}

			//Agrupamos los items en una lista de items

			$itemList = new ItemList();
			$itemList->setItems($variosItems);

			//Detalles de pago, impuestos, envios, etc

			$details = new Details();
			$details->setShipping($datos["tasasEnvio"])
				    ->setTax($datos["impuesto"])
				    ->setSubtotal($datos["subTotal"]);

			//Definimos el pago total con sus detalles

		    $amount = new Amount();
			$amount->setCurrency($datos["divisa"])
			       ->setTotal($datos["total"])
			       ->setDetails($details);

	        //Caracteristicas de la transaccion

	        $transaction = new Transaction();
			$transaction->setAmount($amount)
					    ->setItemList($itemList)
					    ->setDescription("Descripcion de pago")
					    ->setInvoiceNumber(uniqid());

		    //Agregamos la URL despues de realizar el pago, o cuando el pago es cancelado
			#Importante agregar la URL principal de la API developers de paypal

		    $url = Ruta::ctrRuta();

			$redirectUrls = new RedirectUrls();
			$redirectUrls->setReturnUrl("$url/index.php?ruta=finalizar-compra&paypal=true&productos=".$idProductos."&cantidad=".$cantidadProductos."&pago=".$pagoProductos)
			  			 ->setCancelUrl("$url/carrito-de-compras");

  			//Agregar Caracteristicas de pago

  			$payment = new Payment();
			$payment->setIntent("sale")
				    ->setPayer($payer)
				    ->setRedirectUrls($redirectUrls)
				    ->setTransactions(array($transaction));
		    
		    //Trata de ejecutar un proceso y si falla ejecutar una rutina de error

		    try{

		    	//Traemos las credenciales $apiContext (bootstrap.php)
		    	$payment->create($apiContext);

		    }catch(Paypal\Exception\PaypalConnectionException $ex){

		    	echo $ex->getCode(); //Imprime el codigo de error
		    	echo $ex->getData(); //Imprime el detalle del error
		    	die($ex);
		    	return "$url/error";

		    }

		    //Utilizaremos un foreach para iterar sobre $payment, utilizaremos el metodo llamado getLinks() para obtener todos los enlaces que aparecen en el array $payment y caso de que $link->getRel() coincida con 'approval_url' extraemos dicho enlace, finalmente enviamos al usuario a esa direccion que guardamos en la variable $redicrectUrl en el metodo getHref();

		    foreach ($payment->getLinks() as $link) {
		    	
		    	if($link->getRel() == 'approval_url'){

		    		$redirectUrl = $link->getHref();

		    	}

		    }

		    return $redirectUrl;
		}

	}