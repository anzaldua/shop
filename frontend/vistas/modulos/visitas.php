<?php 
	
	//https://www.browserling.com/tools/random-ip

	//$ip = ($_SERVER['REMOTE_ADDR']);

    $ip = "78.48.5.123";

    //http://geoplugin.net/

    $informacionPais = file_get_contents("http://geoplugin.net/json.gp?ip=".$ip);

 	$datosPais = json_decode($informacionPais);

 	$pais = $datosPais->geoplugin_countryName;
 	$codigo = $datosPais->geoplugin_countryCode;

 	$enviarIP = ControladorVisitas::ctrEnviarIP($ip, $pais, $codigo);

 	$totalVisitas = ControladorVisitas::ctrMostrarTotalVisitas();

 ?>


	<!-- Modulo Visitas -->

<div class="container-fluid">
	
	<div class="container">
		
		<div class="row">
			
			<ul  class="breadcrumb lead">
				
				<h2 class="text-center"><small style="font-size: 30px">Tu eres nuestro visitante # <?php echo $totalVisitas["total"] ?></small></h2>
				
				<div class="container-fluid visitas">
					
					<div class="container">

						<div class="row">

							<div class="col-xs-12">
							
								<h1><small>Paises que mas Visitan Nuestro Sitio</small></h1>

							<?php 

								$paises = ControladorVisitas::ctrMostrarPaises();

								$coloresPaises = array("#09F","#900","#059","#260","#F09","#02A",);

								$indice = -1;

								foreach ($paises as $key => $value) {

									$promedio = $value["cantidad"] * 100 / $totalVisitas["total"];

									$indice++;
									
									echo '<div class="col-md-2 col-sm-4 col-xs-12 text-center">
								
											<h2 class="text-muted">'.$value["pais"].'</h2>

											<input type="text" class="knob" value="'.round($promedio).'" data-width="90"	data-height="90" data-fgcolor="'.$coloresPaises[$indice].'" data-readonly="true">

											<p class="text-muted text-center" style="font-size: 12px">'.round($promedio).'% de las Visitas</p>

										</div>';
								}

							 ?>

						</div>

					</div>

				</div>

			</ul>

		</div>

	</div>

</div>

