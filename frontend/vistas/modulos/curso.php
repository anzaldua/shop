<!-- VALIDAR SESION -->

<?php

	if(!isset($_SESSION["validarSesion"])){

		echo '<script>window.location = "'.$url.'";</script>';

		exit();

	}

?>

<!-- BreadCrumb curso -->

<div class="container-fluid">
	
	<div class="container">
		
		<div class="row">
			
			<ul  class="breadcrumb fondoBreadcrumb">
				
				<li><a href="<?php echo $url?>">Inicio</a></li>
				<li class="active paginaActiva"><?php  echo $rutas[0]; ?></li>

			</ul>

		</div>

	</div>

</div>

<!-- Traer Curso -->

<?php 

if(isset($rutas[1]) && isset($rutas[2]) && isset($rutas[3])){

	$item = "id";
	$valor = $rutas[1];
	$idUsuario = $rutas[2];
	$idProducto = $rutas[3];

	$confirmarCompra = ControladorUsuarios::ctrMostrarCompras($item, $valor);

	if($confirmarCompra[0]["id_usuario"] == $idUsuario && 
	   $confirmarCompra[0]["id_usuario"] == $_SESSION["id"] &&
	   $confirmarCompra[0]["id_producto"] == $idProducto){

		echo '<h1 class="text-center">Bienvenido al Curso</h1>';

	}else{

		echo '
			<div class="col-12-xs text-center error404">
				
				<h1>404</h1>
				<h2>Oops! No tienes acceso al curso</h2>

			</div>';

	}

}else{

	echo '
			<div class="col-12-xs text-center error404">
				
				<h1>404</h1>
				<h2>Oops! No tienes acceso al curso</h2>

			</div>';

}

	

 ?>

