<!-- BREADCRUMB INFOPRODUCTOS -->

<div class="container-fluid">
	
	<div class="container">
		
		<div class="row">
			
			<ul  class="breadcrumb fondoBreadcrumb">
				
				<li><a href="<?php echo $url?>">Inicio</a></li>
				<li class="active paginaActiva"><?php  echo $rutas[0]; ?></li>

			</ul>

		</div>

	</div>

</div>

<!-- Tabla Carrito de compras -->

<div class="container-fluid">
	
	<div class="container">
		
		<div class="panel panel-default">
			
			<!-- Cabecera Carrito De Compras -->

			<div class="panel-heading cabeceraCarrito">
				
				<div class="col-md-6 col-sm-7 col-xs-12 text-center">
					
					<h3>
						<small>Producto</small>
					</h3>

				</div>

				<div class="col-md-2 col-sm-1 col-xs-0 text-center">
					
					<h3>
						<small>Precio</small>
					</h3>

				</div>

				<div class="col-sm-2 col-xs-0 text-center">
					
					<h3>
						<small>Cantidad</small>
					</h3>

				</div>

				<div class="col-sm-2 col-xs-0 text-center">
					
					<h3>
						<small>Subtotal</small>
					</h3>

				</div>

			</div>

			<!-- Cuerpo carrito de compras -->

			<!-- ITEM 1 -->

			<div class="panel-body cuerpoCarrito">
					
				<div class="row itemCarrito">
						
					<div class="col-sm-1 col-xs-12">
						
						<br>

						<center>
							
							<button class="btn btn-default backColor">
								
								<i class="fa fa-times"></i>

							</button>

						</center>

					</div>

					<div class="col-sm-1 col-xs-12">

						<figure>
								
							<img src="http://localhost:8080/backend/vistas/img/productos/cursos/curso02.jpg" class="img-thumbnail">

						</figure>
	
					</div>

					<div class="col-sm-4 col-xs-12">

						<br>

						<p class="tituloCarritoCompra text-left">Aprender JavaScript desde 0</p>
	
					</div>

					<div class="col-md-2 col-sm-1 col-xs-12">

						<br>

						<p class="precioCarritoCompra text-center">USD $<span>10</span></p>
	
					</div>

					<div class="col-md-2 col-sm-3 col-xs-12">
						
						<br>

						<div class="col-xs-8">

							<center>
							
								<input type="number" class="form-control" min="1" value="1" readonly>

							</center>

						</div>

					</div>

					<div class="col-md-2 col-sm-1 col-xs-4 text-center">
						
						<br>

						<p>
								
							<strong>USD $<span>10</span></strong>

						</p>

					</div>

				</div>

				<div class="clearfix"></div>

				<hr>
					
				<!-- ITEM 2 -->

				<div class="row itemCarrito">
						
					<div class="col-sm-1 col-xs-12">
						
						<br>

						<center>
							
							<button class="btn btn-default backColor">
								
								<i class="fa fa-times"></i>

							</button>

						</center>

					</div>

					<div class="col-sm-1 col-xs-12">

						<figure>
								
							<img src="http://localhost:8080/backend/vistas/img/productos/ropa/ropa04.jpg" class="img-thumbnail">

						</figure>
	
					</div>

					<div class="col-sm-4 col-xs-12">

						<br>

						<p class="tituloCarritoCompra text-left">Vestido jean</p>
	
					</div>

					<div class="col-md-2 col-sm-1 col-xs-12">

						<br>

						<p class="precioCarritoCompra text-center">USD $<span>10</span></p>
	
					</div>

					<div class="col-md-2 col-sm-3 col-xs-12">
						
						<br>

						<div class="col-xs-8">

							<center>
							
								<input type="number" class="form-control" min="1" value="1">

							</center>

						</div>

					</div>

					<div class="col-md-2 col-sm-1 col-xs-4 text-center">
						
						<br>

						<p>
								
							<strong>USD $<span>11</span></strong>

						</p>

					</div>

				</div>

			</div>

			<div class="clearfix"></div>

				<hr>

				<!-- Suma Total de productos -->

				<div class="panel-body sumaCarrito">
						
					<div class="col-md-4 col-sm-6 col-xs-12 pull-right well">
						
						<div class="col-xs-6">
							
							<h4>Total: </h4>

						</div>

						<div class="col-xs-6">
							
							<h4 class="sumaSubTotal">
								
								<strong>USD $<span>21</span></strong>

							</h4>

						</div>

					</div>

				</div>

				<!-- Boton Chekout -->

				<div class="panel-heading cabeceraCheckout">
						
					<button class="btn btn-default btn-lg pull-right">Realizar Pago</button>

				</div>

		</div>

	</div>

</div>