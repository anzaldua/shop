<?php

	$url = Ruta::ctrRuta();
	$servidor = Ruta::ctrRutaServidor();

	/* Inicio de Sesion Usuario */

	if(isset($_SESSION["validarSesion"])){

		if($_SESSION["validarSesion"] == "ok"){

			echo '<script>

					localStorage.setItem("usuario", "'.$_SESSION["id"].'");
			
	       		</script>';

		}

	}
	
	/*===============================================================
	=            Creacion del objeto de la API de google            =
	===============================================================*/
	
	$cliente = new Google_Client();
	$cliente->setAuthConfig('modelos/client_secretgoogle.json');
	$cliente->setAccessType("offline");
	$cliente->setScopes(['profile','email']);

	/*===============================================================
	=         Ruta para el login de Google           =
	===============================================================*/

	$rutaGoogle = $cliente->createAuthUrl();

	/*===============================================================
	=         Recibimos la variable get Code de google           =
	===============================================================*/
	
	if(isset($_GET["code"])){

		$token = $cliente->authenticate($_GET["code"]);

		$_SESSION['id_token_google'] = $token;

		$cliente->setAccessToken($token);
	}

	/*===============================================================
	=         RRecibimos los datos cifrados de google en un array  =
	===============================================================*/

	if($cliente->getAccessToken()){

		$item = $cliente->verifyIdToken();

		$datos = array("nombre"=>$item["name"],
					   "email"=>$item["email"],
					   "foto"=>$item["picture"],
					   "password"=>"null",
					   "modo"=>"google",
					   "verificacion"=>0,
					   "emailEncriptado"=>"null");

		$respuesta = ControladorUsuarios::ctrRegistroRedesSociales($datos);

		echo '<script>
	
					setTimeout(function(){

						window.location = localStorage.getItem("rutaActual");

					})

			  </script>';

		}
?>

<!-- TOP -->

<div class="container-fluid barraSuperior" id="top">
	
	<div class="container">
		
		<div class="row">
			
			<!-- SOCIAL -->

			<div class="col-lg-9 col-md-9 col-sm-8 col-xs-12 social">
				
				<ul>
						
					<?php

						$social = ControladorPlantilla::ctrEstiloPlantilla();

						$jsonRedesSociales = json_decode($social["redesSociales"], true);

						foreach ($jsonRedesSociales as $key => $value) {
							echo'
								<li>
									<a href="'.$value["url"].'" target="_blank">
										<i class="fa '.$value["red"].' redSocial '.$value["estilo"].'" aria-hidden="true"></i>
									</a>
								</li>';
						}

					?>

				</ul>

			</div>

			<!-- REGISTRO -->
			
			<div class="col-lg-3 col-md-3 col-sm-4 col-xs-12 registro">

				<ul>

					<?php

						if(isset($_SESSION["validarSesion"])){

							if($_SESSION["validarSesion"] == "ok"){

								if($_SESSION["modo"] == "directo"){

									if($_SESSION["foto"] != ""){

										echo '<li>
											
												<img src="'.$url.$_SESSION["foto"].'" width="10%" class="img-circle">

											  </li>';

									}else{

										echo '<li>
											
												<img src="'.$servidor.'vistas/img/usuarios/default/anonymous.png" width="10%" class="img-circle">

											  </li>';

									}

									echo '<li>|</li>
									  <li><a href="'.$url.'perfil">Ver Perfil</a></li>
									  <li>|</li
									  <li><a href="'.$url.'salir">Salir</a></li';

								

								}else if($_SESSION["modo"] == "facebook"){


									echo '<li>
											
											<img src="'.$_SESSION["foto"].'" width="10%" class="img-circle">

										  </li>

										  <li>|</li>
										  <li><a href="'.$url.'perfil">Ver Perfil</a></li>
										  <li>|</li
										  <li><a class="salirFacebook" href="'.$url.'salir">Salir</a></li';

								}else if($_SESSION["modo"] == "google"){

									echo '<li>
											
											<img src="'.$_SESSION["foto"].'" width="10%" class="img-circle">

										  </li>

										  <li>|</li>
										  <li><a href="'.$url.'perfil">Ver Perfil</a></li>
										  <li>|</li
										  <li><a href="'.$url.'salir">Salir</a></li';


								}

								

							}

						}else{

							echo '<li><a href="#modalIngreso" data-toggle="modal">Ingresar</a></li>
								  <li>|</li>
								  <li><a href="#modalRegistro" data-toggle="modal">Crear una cuenta</a></li>';

						}

					?>
					
					

				</ul>

			</div>

		</div>

	</div>

</div>

<!-- HEADER -->

<header class="container-fluid">
	
	<div class="container">
		
		<div class="row" id="cabezote">
			
			<!-- LOGOTIPO -->
			
			<div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">
				
				<a href="<?php echo $url?>">
					
					<img src="<?php echo $servidor.$social["logo"];?>" alt="">

				</a>

			</div>

			<!-- BLOQUE CATEGORIAS Y BUSCADOR -->

			<div class="col-lg-6 col-md-6 col-sm-8 col-xs-12">
				
				<!-- BOTON CATEGORIAS -->
				
				<div class="col-lg-4 col-md-4 col-sm-4 col-xs-12 backColor" id="btnCategorias">

					<p>CATEGORIAS
					<span class="pull-right"><i class="fa fa-bars" aria-hidden="true"></i></span>
					</p>
				</div>

				<!-- BUSCADOR -->

				<div class="input-group col-lg-8 col-md-8 col-sm-8 col-xs-12" id="buscador">
					
					<input type="search" name="buscar" class="form-control" placeholder="Buscar...">

					<span class="input-group-btn">
						
						<a href="<?php echo $url; ?>buscador/1/recientes">
							
							<button class="btn btn-default backColor" type="submit">
								<i class="fa fa-search"></i>
							</button>

						</a>

					</span>

				</div>

			</div>

			<!-- CARRITO -->

			<div class="col-lg-3 col-md-3 col-sm-2 col-xs-12" id="carrito">
				
				<a href="<?php echo $url;?>carrito-de-compras">
					
					<button class="btn btn-default pull-left backColor">
						<i class="fa fa-shopping-cart" aria-hidden="true"></i>
					</button>

				</a>

				<p>TU CESTA <span class="cantidadCesta"></span><br> USD $ <span class="sumaCesta"></span></p>

			</div>
			
		</div>

		<!-- CATEGORIAS -->

		<div class="col-xs-12 backColor" id="categorias">

			<?php

				$item = null;
				$valor = null;

				$categorias = ControladorProductos::ctrMostrarCategorias($item, $valor);

				

				foreach ($categorias as $key => $value) {

					echo '
						<div class="col-lg-2 col-md-3 col-sm-4 col-xs-12">
				
							<h4>
								<a href="'.$url.$value["ruta"].'" class="pixelCategoria" titulo="'.$value["categoria"].'">'.$value["categoria"].'</a>
							</h4>

							<hr>

							<ul>';

							$item = "id_categoria";

							$valor = $value["id"];

							$subcategorias = ControladorProductos::ctrMostrarSubCategorias($item, $valor);

							foreach ($subcategorias as $key => $value) {
								echo '<li><a href="'.$url.$value["ruta"].'" class="pixelSubCategoria" titulo="'.$value["subcategoria"].'">'.$value["subcategoria"].'</a></li>';
							}
								
							echo'</ul>

							</div>';
				}

			?>

		</div>

	</div>

</header>

<!-- VENTANA MODAL PARA EL REGISTRO -->


	<!-- Modal -->
  <div class="modal fade modalFormulario" id="modalRegistro" role="dialog">

    <div class="modal-content modal-dialog">

        <div class="modal-body modalTitulo">

		  <h3 class="backColor">Registro</h3>

          <button type="button" class="close" data-dismiss="modal">&times;</button>

          <!-- REGISTRO EN FACEBOOK -->

			<div class="col-sm-6 col-xs-12 facebook">
				
				<p>
					<i class="fa fa-facebook"></i>
					Registro con Facebook
				</p>

			</div>

          <!-- REGISTRO EN GOOGLE -->
		  <a href="<?php echo $rutaGoogle; ?>">
	          <div class="col-sm-6 col-xs-12 google">
					
					<p>
						<i class="fa fa-google"></i>
						Registro con Google
					</p>

			  </div>
		  </a>

			<!-- REGISTRO DIRECTO -->


			<form method="post" onsubmit="return registroUsuario()">
				
				<hr>

				<div class="form-group">
					
					<div class="input-group">
							
						<span class="input-group-addon">

							<i class="glyphicon glyphicon-user"></i>

						</span>

						<input type="text" class="form-control" id="regUsuario" name="regUsuario" placeholder="Nombre Completo" required>

					</div>

				</div>

				<div class="form-group">
					
					<div class="input-group">
							
						<span class="input-group-addon">

							<i class="glyphicon glyphicon-envelope"></i>

						</span>

						<input type="email" class="form-control" id="regEmail" name="regEmail" placeholder="Correo Electronico" required>

					</div>

				</div>

				<div class="form-group">
					
					<div class="input-group">
							
						<span class="input-group-addon">

							<i class="glyphicon glyphicon-lock"></i>

						</span>

						<input type="password" class="form-control" id="regPassword" name="regPassword" placeholder="Contraseña" required>

					</div>

				</div>

				<!-- POLICITAS DE PRIVACIDAD https://www.iubenda.com -->

				<div class="checkbox">
						
					<label>
						
						<input type="checkbox" id="regPoliticas">

						<small>
							
							Acepta nuestras condiciones de uso y politicas de privacidad

							<br>

							<a href="https://www.iubenda.com/privacy-policy/41232183" class="iubenda-white iubenda-embed" title="Condiciones de uso y politicas de privacidad">Leer Mas</a><script type="text/javascript">(function (w,d) {var loader = function () {var s = d.createElement("script"), tag = d.getElementsByTagName("script")[0]; s.src="https://cdn.iubenda.com/iubenda.js"; tag.parentNode.insertBefore(s,tag);}; if(w.addEventListener){w.addEventListener("load", loader, false);}else if(w.attachEvent){w.attachEvent("onload", loader);}else{w.onload = loader;}})(window, document);</script>

						</small>

					</label>

				</div>

				<?php

					$registro = new ControladorUsuarios();
					$registro -> ctrRegistroUsuario();

				?>
				
				<input type="submit" class="btn btn-default backColor btn-block" value="Enviar">
				

			</form>

        </div>

        <div class="modal-footer">

          ¿Ya tienes una cuenta registrada? | <strong><a href="#modalIngreso" data-dismiss="modal" data-toggle="modal">Ingresar</a></strong>

        </div>
      
    </div>

  </div>

  <!-- VENTANA MODAL PARA EL INGRESO -->

	<!-- Modal -->
  <div class="modal fade modalFormulario" id="modalIngreso" role="dialog">

    <div class="modal-content modal-dialog">

        <div class="modal-body modalTitulo">

		  <h3 class="backColor">Ingresar</h3>

          <button type="button" class="close" data-dismiss="modal">&times;</button>

          <!-- INGRESO CON FACEBOOK -->

			<div class="col-sm-6 col-xs-12 facebook">
				
				<p>
					<i class="fa fa-facebook"></i>
					Ingreso con Facebook
				</p>

			</div>

          <!-- INGRESO CON GOOGLE -->
		  <a href="<?php echo $rutaGoogle; ?>">
	          <div class="col-sm-6 col-xs-12 google">
					
					<p>
						<i class="fa fa-google"></i>
						Ingreso con Google
					</p>

			  </div>
		  </a>

			<!-- REGISTRO DIRECTO -->


			<form method="post">
				
				<hr>


				<div class="form-group">
					
					<div class="input-group">
							
						<span class="input-group-addon">

							<i class="glyphicon glyphicon-envelope"></i>

						</span>

						<input type="email" class="form-control" id="ingEmail" name="ingEmail" placeholder="Correo Electronico" required>

					</div>

				</div>

				<div class="form-group">
					
					<div class="input-group">
							
						<span class="input-group-addon">

							<i class="glyphicon glyphicon-lock"></i>

						</span>

						<input type="password" class="form-control" id="ingPassword" name="ingPassword" placeholder="Contraseña" required>

					</div>

				</div>

				<?php

					$ingreso = new ControladorUsuarios();
					$ingreso -> ctrIngresoUsuario();

				?>
				
				<input type="submit" class="btn btn-default backColor btn-block btnIngreso" value="Ingresar">

				<br>

				<center>

					<a href="#modalPassword" data-dismiss="modal" data-toggle="modal">¿Olvidaste tu Contraseña?</a>

				</center>
				

			</form>

        </div>

        <div class="modal-footer">

          ¿No tienes una cuenta registrada? | <strong><a href="#modalRegistro" data-dismiss="modal" data-toggle="modal">Registrate!!</a></strong>

        </div>
      
    </div>

  </div>


   <!-- VENTANA MODAL PARA EL OLVIDO DE PASSWORD -->

	<!-- Modal -->
  <div class="modal fade modalFormulario" id="modalPassword" role="dialog">

    <div class="modal-content modal-dialog">

        <div class="modal-body modalTitulo">

		  <h3 class="backColor">Solicitud de nueva Contraseña </h3>

          <button type="button" class="close" data-dismiss="modal">&times;</button>

          
			<!-- OLVIDO DE Contraseña -->


			<form method="post">

				<hr>

				<label class="text-center text-muted">Escribe el correo electronico con el que te registraste y alli te enviaremos una nueva Contraseña</label>
				
				<div class="form-group">
					
					<div class="input-group">
							
						<span class="input-group-addon">

							<i class="glyphicon glyphicon-envelope"></i>

						</span>

						<input type="email" class="form-control" id="passEmail" name="passEmail" placeholder="Correo Electronico" required>

					</div>

				</div>

				<?php

					$password = new ControladorUsuarios();
					$password -> ctrOlvidoPassword();

				?>
				
				<input type="submit" class="btn btn-default backColor btn-block btnIngreso" value="Solicitar Cambio">
				

			</form>

        </div>

        <div class="modal-footer">

          ¿No tienes una cuenta registrada? | <strong><a href="#modalRegistro" data-dismiss="modal" data-toggle="modal">Registrate!!</a></strong>

        </div>
      
    </div>

  </div>


