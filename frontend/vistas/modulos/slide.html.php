<!-- SLIDE SHOW -->

<div class="container-fluid" id="slide">
	
	<div class="row">
		
		<!-- DIAPOSITIVAS -->

		<ul>
			<!-- SLIDE 1  -->
			<li>
				
				<img src="../backend/vistas/img/slide/default/back_default.jpg" alt="">

				<div class="slideOpciones slideOpcion1">
						
					<img class="imgProducto" src="../backend/vistas/img/slide/slide1/calzado.png" alt="" style="top:15%; right:10%; width:45%">

					<div class="textosSlide" style="top:20%; left:10%; width:40%;">
						
						<h1 style="color:#333;">Lorem Impsum</h1>
						<h2 style="color:#777;">Lorem ipsum dolor sit amet</h2>
						<h3 style="color:#888;">Lorem ipsum dolor sit amet</h3>

						<a href="#">
							
							<button class="btn btn-default backColor">

							Ver Producto <span class="fa fa-chevron-right"></span>	

							</button>

						</a>

					</div>

				</div>

			</li>

			<!-- SLIDE 2  -->
			<li>
				
				<img src="../backend/vistas/img/slide/default/back_default.jpg" alt="">

				<div class="slideOpciones slideOpcion2">
						
					<img class="imgProducto" src="../backend/vistas/img/slide/slide2/curso.png" alt="" style="top:5%; left:15%; width:25%">

					<div class="textosSlide" style="top:20%; right:10%; width:40%;">
						
						<h1 style="color:#333";>Lorem Impsum</h1>
						<h2 style="color:#777";>Lorem ipsum dolor sit amet</h2>
						<h3 style="color:#888";>Lorem ipsum dolor sit amet</h3>

						<a href="#">
							
							<button class="btn btn-default backColor">

							Ver Producto <span class="fa fa-chevron-right"></span>	

							</button>

						</a>

					</div>

				</div>

				<!-- SLIDE 3  -->
			<li>
				
				<img src="../backend/vistas/img/slide/slide3/fondo2.jpg" alt="">

				<div class="slideOpciones slideOpcion2">
						
					<img class="imgProducto" src="../backend/vistas/img/slide/slide3/iphone.png" alt="" style="top:5%; left:15%; width:25%">

					<div class="textosSlide" style="top:20%; right:10%; width:40%;">
						
						<h1 style="color:#fff;">Lorem Impsum</h1>
						<h2 style="color:#fff;">Lorem ipsum dolor sit amet</h2>
						<h3 style="color:#fff;">Lorem ipsum dolor sit amet</h3>

						<a href="#">
							
							<button class="btn btn-default backColor">

							Ver Producto <span class="fa fa-chevron-right"></span>	

							</button>

						</a>

					</div>

				</div>

			</li>

			<!-- SLIDE 4  -->
			<li>
				
				<img src="../backend/vistas/img/slide/slide4/fondo3.jpg" alt="">

				<div class="slideOpciones slideOpcion1">
						
					<img class="imgProducto" src="" alt="">

					<div class="textosSlide" style="top:20%; left:10%; width:40%;">
						
						<h1 style="color:#333";>Lorem Impsum</h1>
						<h2 style="color:#777";>Lorem ipsum dolor sit amet</h2>
						<h3 style="color:#888";>Lorem ipsum dolor sit amet</h3>

						<a href="#">
							
							<button class="btn btn-default backColor">

							Ver Producto <span class="fa fa-chevron-right"></span>	

							</button>

						</a>

					</div>

				</div>

			</li>

		</ul>

		<!-- PAGINACION -->

		<ol id="paginacion">
			
			<li item="1"><span class="fa fa-circle"></span></li>
			<li item="2"><span class="fa fa-circle"></span></li>
			<li item="3"><span class="fa fa-circle"></span></li>
			<li item="4"><span class="fa fa-circle"></span></li>

		</ol>

		<!-- FLECHAS -->

		<div class="flechas" id="retroceder"><span class="fa fa-chevron-left"></span></div>
		<div class="flechas" id="avanzar"><span class="fa fa-chevron-right"></span></div>

	</div>

</div>

<center>
	
	<button id="btnSlide" class="backColor">
		
		<i class="fa fa-angle-up"></i>

	</button>

</center>