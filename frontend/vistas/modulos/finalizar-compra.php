<?php

	$url = Ruta::ctrRuta();

	if(!isset($_SESSION["validarSesion"])){

		echo '<script>window.location = "'.$url.'";</script>';

		exit();

	}

	//Requerimos las credenciales de paypal
	require 'extensiones/bootstrap.php';
	require_once "modelos/carrito.modelo.php";

	//Importamos la libreria del SDK
	use PayPal\Api\Payment;
	use PayPal\Api\PaymentExecution;

	/*===================================
	=            Pago Paypal            =
	===================================*/

	//Evaluamos si la compra esta aprobada

	if(isset($_GET['paypal']) && $_GET['paypal'] === "true"){
		
		//recibo los productos comprados
		$productos = explode("-", $_GET["productos"]);
		$cantidad = explode("-", $_GET["cantidad"]);
		$pago = explode("-", $_GET["pago"]);

		//Capturamos el id del pago que arroja Paypal
		$paymentId = $_GET["paymentId"];

		//Creamos un objeto de payment para confirmar que las credenciales si tengan el id de pago resuelto
		$payment = Payment::get($paymentId, $apiContext);

		//Creamos la ejecucion de pago, invocando la clase de PaymentExecution() y extraemos el id del pagador
		$execution = new PaymentExecution();
		$execution-> setPayerId($_GET["PayerID"]);

		//Validamos con las credenciales que el id del pagador si coincida
		$payment->execute($execution, $apiContext);
		
		$datosTransaccion = $payment->toJSON();

	    $datosUsuario = json_decode($datosTransaccion);

	    $emailComprador = $datosUsuario->payer->payer_info->email;
	    $direccion = $datosUsuario->payer->payer_info->shipping_address->line1;
	    $ciudad = $datosUsuario->payer->payer_info->shipping_address->city;
	    $estado = $datosUsuario->payer->payer_info->shipping_address->state;
	    $codigoPostal = $datosUsuario->payer->payer_info->shipping_address->postal_code;
	    $pais = $datosUsuario->payer->payer_info->shipping_address->country_code;

	    $direccionCompleta = $direccion.", ".$ciudad.", ".$estado.", ".$codigoPostal; 

		//Actualizamos la base de datos
		
		for ($i=0; $i < count($productos); $i++) { 
			
			$datos = array("idUsuario"=>$_SESSION["id"],
						   "idProducto"=>$productos[$i],
							"metodo"=>"paypal",
							"email"=>$emailComprador,
							"direccion"=>$direccionCompleta,
							"pais"=>$pais,
							"pago"=>$pago[$i]);

			$respuesta = ControladorCarrito::ctrNuevasCompras($datos);

			$ordenar = "id";
			$item = "id";
			$valor = $productos[$i];

			$productosCompra = ControladorProductos::ctrListarProductos($ordenar, $item, $valor);

			foreach ($productosCompra as $key => $value) {
				
				$item1 = "ventas";
				$valor1 = $value["ventas"] + $cantidad[$i];

				$item2 = "id";
				$valor2 = $value["id"];

				$actualizarCompra = ControladorProductos::ctrActualizarProducto($item1, $valor1, $item2, $valor2);

			}

			if($respuesta == "ok" && $actualizarCompra == "ok"){

				//Elimina los productos en el carrito y redirige al perfil
				echo '<script>

						localStorage.removeItem("listaProductos");
						localStorage.removeItem("cantidadCesta");
						localStorage.removeItem("sumaCesta");

						window.location = "'.$url.'perfil";

					</script>';

			}

		}
			
	}


	//ADQUISICIONES GRATUITAS

	else if(isset($_GET["gratis"]) && $_GET["gratis"] === 'true'){

		$producto = $_GET["producto"];
		$titulo = $_GET["titulo"];

		$datos = array("idUsuario"=>$_SESSION["id"],
					   "idProducto"=>$producto,
						"metodo"=>"gratis",
						"email"=>$_SESSION["email"],
						"direccion"=>"",
						"pais"=>"");

		$respuesta = ControladorCarrito::ctrNuevasCompras($datos);

		$ordenar = "id";
		$item = "id";
		$valor = $producto;

		$productosGratis = ControladorProductos::ctrListarProductos($ordenar, $item, $valor);

		foreach ($productosGratis as $key => $value) {
			
			$item1 = "ventasGratis";
			$valor1 = $value["ventasGratis"] + 1;

			$item2 = "id";
			$valor2 = $value["id"];

			$actualizarSolicitud = ControladorProductos::ctrActualizarProducto($item1, $valor1, $item2, $valor2);
		}

		if($respuesta == "ok" && $actualizarSolicitud == "ok"){

			echo '<script>

					window.location = "'.$url.'ofertas/aviso";	

				</script>'; 

		} 

	}else{

		echo '<script>window.location = "'.$url.'cancelar";</script>';

	}
	