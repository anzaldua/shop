<?php

	$url = Ruta::ctrRuta();
	$servidor = Ruta::ctrRutaServidor();
	
?>

<!-- BREADCRUMB Ofertas -->

<div class="container-fluid">
	
	<div class="container">
		
		<div class="row">
			
			<ul  class="breadcrumb fondoBreadcrumb">
				
				<li><a href="<?php echo $url?>">Inicio</a></li>
				<li class="active paginaActiva"><?php  echo $rutas[0]; ?></li>

			</ul>

		</div>

	</div>

</div>

<!-- Jumbotron Aviso Oferta -->

<?php

	if(isset($rutas[1]) == "aviso"){

		echo '<div class="container-fluid">
	
				<div class="container">
					
					<div class="jumbotron">

						<button type="button" class="close cerrarOfertas" style="margin-top:-50px;"><h1>&times;</h1></button>
						
						<h1 class="text-center">Hola!</h1>

						<p class="text-center">Tu articulo ha sido asignado a tus compras pero antes queremos presentarte las siguientes ofertas, si no deseas ver las ofertas y deseas continuar en el articulo que acabas de adquirir haz click en el siguiente boton:
							<br>
							<br>
							
							<a href="'.$url.'perfil">
								<button class="btn btn-default backColor btn-lg">
									Ver articulos comprados
								</button>
							</a>

							<br>

							<a href="#moduloOfertas">
								<button class="btn btn-default backColor btn-lg">
									Ver Ofertas
								</button>
							</a>
				
						</p>

					</div>

				</div>

			</div>';

	}

?>

<div class="container-fluid">
	
	<div class="container">
		
		<div class="row" id="moduloOfertas">
			
			<?php 

				$item = null;
				$valor = null;

				date_default_timezone_set("America/Mexico_City");

				$fecha = date('Y-m-d');
				$hora = date('H:i:s');

				$fechaBDOferta = $value["finOferta"];

				$fechaActual = $fecha.' '.$hora;
				

				/*========================================================
				=            Traemos las categorias ofertadas            =
				========================================================*/
				
				$respuesta = ControladorProductos::ctrMostrarCategorias($item, $valor);

				foreach ($respuesta as $key => $value) {
					
					if($value["oferta"] == 1){

						if($value["finOferta"] > $fecha){

							$datetime1 = new DateTime($fechaBDOferta);

							$datetime2 = new DateTime($fechaActual);

							$interval = date_diff($datetime1, $datetime2);

							$finOferta = $interval->format('%a');

						echo '<div class="col-md-4 col-sm-6 col-xs-12">

								<div class="ofertas">

									<h3 class="text-center text-uppercase">

										Oferta especial en <br>'.$value["categoria"].'

									</h3>

									<figure>

										<img src="'.$servidor.$value["imgOferta"].'" class="img-responsive" width="100%">

										<div class="sombraSuperior"></div>';

										if($value["descuentoOferta"] != 0){

											echo '<h1 class="text-center text-uppercase">% '.$value["descuentoOferta"].'-Off</h1>';

										}else{

											echo '<h1 class="text-center text-uppercase">$ '.$value["precioOferta"].'-Off</h1>';

										}

								echo '</figure>';

								if($finOferta == 0){

									echo '<h4 class="text-center">La Oferta termina Hoy!!</h4>';

								}else if($finOferta == 1){

									echo '<h4 class="text-center">La Oferta termina en '.$finOferta.' dia!!</h4>';

								}else{

									echo '<h4 class="text-center">La Oferta termina en '.$finOferta.' dias!!</h4>';

								}
			
							echo '<center>

									<div class="countdown" finOferta="'.$fechaBDOferta.'"></div>
	
									<a href="'.$url.$value["ruta"].'" class="pixelOferta" titulo="'.$value["categoria"].'">

									<button class="btn backColor btn-lg text-uppercase">Ir a la Oferta</button></a>
	
								 </center>

								</div>

							</div>';
							
						}

					}

				}

				/*========================================================
				=            Traemos las Subcategorias ofertadas            =
				========================================================*/
				
				$respuestaSubcategorias = ControladorProductos::ctrMostrarSubCategorias($item, $valor);

				foreach ($respuestaSubcategorias as $key => $value) {
					
					if($value["oferta"] == 1 && $value["ofertadoPorCategoria"] == 0){

						if($value["finOferta"] > $fecha){

							$datetime1 = new DateTime($fechaBDOferta);

							$datetime2 = new DateTime($fechaActual);

							$interval = date_diff($datetime1, $datetime2);

							$finOferta = $interval->format('%a');

						echo '<div class="col-md-4 col-sm-6 col-xs-12">

								<div class="ofertas">

									<h3 class="text-center text-uppercase">

										Oferta especial en <br>'.$value["subcategoria"].'

									</h3>

									<figure>

										<img src="'.$servidor.$value["imgOferta"].'" class="img-responsive" width="100%">

										<div class="sombraSuperior"></div>';

										if($value["descuentoOferta"] != 0){

											echo '<h1 class="text-center text-uppercase">% '.$value["descuentoOferta"].'-Off</h1>';

										}else{

											echo '<h1 class="text-center text-uppercase">$ '.$value["precioOferta"].'-Off</h1>';

										}

								echo '</figure>';

								if($finOferta == 0){

									echo '<h4 class="text-center">La Oferta termina Hoy!!</h4>';

								}else if($finOferta == 1){

									echo '<h4 class="text-center">La Oferta termina en '.$finOferta.' dia!!</h4>';

								}else{

									echo '<h4 class="text-center">La Oferta termina en '.$finOferta.' dias!!</h4>';

								}
			
							echo '<center>

									<div class="countdown" finOferta="'.$fechaBDOferta.'"></div>
	
									<a href="'.$url.$value["ruta"].'" class="pixelOferta">

									<button class="btn backColor btn-lg text-uppercase">Ir a la Oferta</button></a>
	
								 </center>

								</div>

							</div>';
							
						}

					}

				}

				/*========================================================
				=            Traemos los productos ofertados            =
				========================================================*/

				$ordenar = "id";
				
				$respuestaProductos = ControladorProductos::ctrListarProductos($ordenar ,$item, $valor);

				foreach ($respuestaProductos as $key => $value) {
					
					if($value["oferta"] == 1 && $value["ofertadoPorCategoria"] == 0 && $value["ofertadoPorSubCategoria"] == 0){

						if($value["finOferta"] > $fecha){

							$datetime1 = new DateTime($fechaBDOferta);

							$datetime2 = new DateTime($fechaActual);

							$interval = date_diff($datetime1, $datetime2);

							$finOferta = $interval->format('%a');

						echo '<div class="col-md-4 col-sm-6 col-xs-12">

								<div class="ofertas">

									<h3 class="text-center text-uppercase">

										Oferta especial en <br>'.$value["titulo"].'

									</h3>

									<figure>

										<img src="'.$servidor.$value["imgOferta"].'" class="img-responsive" width="100%">

										<div class="sombraSuperior"></div>';

										if($value["descuentoOferta"] != 0){

											echo '<h1 class="text-center text-uppercase">% '.$value["descuentoOferta"].'-Off</h1>';

										}else{

											echo '<h1 class="text-center text-uppercase">$ '.$value["precioOferta"].'-Off</h1>';

										}

								echo '</figure>';

								if($finOferta == 0){

									echo '<h4 class="text-center">La Oferta termina Hoy!!</h4>';

								}else if($finOferta == 1){

									echo '<h4 class="text-center">La Oferta termina en '.$finOferta.' dia!!</h4>';

								}else{

									echo '<h4 class="text-center">La Oferta termina en '.$finOferta.' dias!!</h4>';

								}
			
							echo '<center>

									<div class="countdown" finOferta="'.$fechaBDOferta.'"></div>
	
									<a href="'.$url.$value["ruta"].'" class="pixelOferta">

									<button class="btn backColor btn-lg text-uppercase">Ir a la Oferta</button></a>
	
								 </center>

								</div>

							</div>';
							
						}

					}

				}
				

			 ?>

		</div>

	</div>

</div>

