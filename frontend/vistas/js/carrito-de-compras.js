/*=========================================*/
/*=========================================*/
/*=========================================*/
/*=========================================*/
/*=========================================*/
/*=========================================*/
/*=========================================*/
/* VISUALIZAR LA CESTA DEL CARRITO DE COMPRAS           
/*=========================================*/

if(localStorage.getItem("cantidadCesta") != null){

	$(".cantidadCesta").html(localStorage.getItem("cantidadCesta"));
	$(".sumaCesta").html(localStorage.getItem("sumaCesta"));

}else{

	$(".cantidadCesta").html("0");
	$(".sumaCesta").html("0");

}

/*=========================================*/
/*=========================================*/
/*=========================================*/
/*=========================================*/
/*=========================================*/
/*=========================================*/
/*=========================================*/
/* VISUALIZAR LOS PRODUCTOS EN LA PAGINA CARRITO DE COMPRAS           
/*=========================================*/

if(localStorage.getItem("listaProductos") != null){

	var listaCarrito = JSON.parse(localStorage.getItem("listaProductos"));	

	listaCarrito.forEach(functionForEach);

	function functionForEach(item, index){

		var datosProducto = new FormData();
		var precio = 0;

		datosProducto.append("id", item.idProducto);

		$.ajax({

			url: rutaOculta+"ajax/producto.ajax.php",
				type: "POST",
				data: datosProducto,
				cache: false,
				contentType: false,
				processData: false,
				dataType: "json",
				success: function(respuesta){

					if(respuesta["precioOferta"] == 0){

						precio = respuesta["precio"];

					}else{

						precio = respuesta["precioOferta"];

					}
				
					$(".cuerpoCarrito").append(
					
						'<div class="row itemCarrito">'+
									
								'<div class="col-sm-1 col-xs-12">'+
									
									'<br>'+

									'<center>'+
										
										'<button class="btn btn-default backColor quitarItemCarrito"  idProducto="'+item.idProducto+'" peso="'+item.peso+'">'+
											
											'<i class="fa fa-times"></i>'+

										'</button>'+

									'</center>'+

								'</div>'+

								'<div class="col-sm-1 col-xs-12">'+

									'<figure>'+
											
										'<img src="'+item.imagen+'" class="img-thumbnail">'+

									'</figure>'+

								'</div>'+

								'<div class="col-sm-4 col-xs-12">'+

									'<br>'+

									'<p class="tituloCarritoCompra text-left">'+item.titulo+'</p>'+

								'</div>'+

								'<div class="col-md-2 col-sm-1 col-xs-12">'+

									'<br>'+

									'<p class="precioCarritoCompra text-center">USD $<span>'+precio+'</span></p>'+

								'</div>'+

								'<div class="col-md-2 col-sm-3 col-xs-12">'+
									
									'<br>'+

									'<div class="col-xs-8">'+

										'<center>'+
										
											'<input type="number" class="form-control cantidadItem" min="1" value="'+item.cantidad+'" tipo="'+item.tipo+'" precio="'+item.precio+'"'+
											'idProducto="'+item.idProducto+'" item="'+index+'">'+

										'</center>'+

									'</div>'+

								'</div>'+

								'<div class="col-md-2 col-sm-1 col-xs-4 text-center">'+
									
									'<br>'+

									'<p class="subTotal'+index+' subTotales">'+
											
										'<strong>USD $<span>'+Number(item.cantidad)*Number(precio)+'</span></strong>'+

									'</p>'+

								'</div>'+

							'</div>'+

							'<div class="clearfix"></div>'+

							'<hr>');

					/* EVITAR MANIPULAR LA CANTIDAD EN PRODUCTOS VIRTUALES */
					
					$(".cantidadItem[tipo='virtual']").attr("readonly", "true");

					/*=======================================================================
					/*=======================================================================
					/*=======================================================================
					/*=======================================================================
					/*=======================================================================
					/*=======================================================================
					=                        ACTUALIZAR SUBTOTAL                            =
					=======================================================================*/

					var precioCarritoCompra = $(".cuerpoCarrito .precioCarritoCompra span");

					cestaCarrito(precioCarritoCompra.length);

					sumaSubtotales();

				}
			  })

	}

}else{

	$(".cuerpoCarrito").html('<div class="well text-center backColor">Aun no hay productos en el carrito de compras.</div>');
	$(".sumaCarrito").hide();
	$(".cabeceraCheckout").hide();

}

/*=========================================*/
/*=========================================*/
/*=========================================*/
/*=========================================*/
/*=========================================*/
/*=========================================*/
/*=========================================*/
/* 			 AGREGAR AL CARRITO            */
/*=========================================*/


$(".agregarCarrito").click(function(){ 

	var idProducto = $(this).attr("idProducto");
	var imagen = $(this).attr("imagen");
	var titulo = $(this).attr("titulo");
	var precio = $(this).attr("precio");
	var tipo = $(this).attr("tipo");
	var peso = $(this).attr("peso");

	var agregarAlCarrito = false;

	/*=========================================
	=            CAPTURAR DETALLES            =
	=========================================*/

	if(tipo == "virtual"){

		agregarAlCarrito = true;

	}else{

		var seleccionarDetalleTalla = $(".seleccionarDetalleTalla");
		var seleccionarDetalleColor = $(".seleccionarDetalleColor")

		for (var i = 0; i < seleccionarDetalleTalla.length; i++) {
			
			if($(seleccionarDetalleTalla[i]).val() == "" || $(seleccionarDetalleColor[i]).val() == ""){

				swal({
								
					title: "Debe seleccionar talla y color",
					text: "Para agregar un producto a su carrito de compras, debe elegir talla y color",
					type: "warning",
					confirmButtonText: "Cerrar",
					closeOnConfirm: false

				});

			}else{

				titulo = titulo + " " + $(seleccionarDetalleTalla[i]).val() + " " + $(seleccionarDetalleColor[i]).val();

				agregarAlCarrito = true;

			}

		}

	}
	
	/*======================================================================================
	=            ALMACENAR EN EL LOCALSTORAGE LOS PRODUCTOS AGREGADOS AL CARRITO            =
	======================================================================================*/

	if(agregarAlCarrito){

	/*=================================================================
	=            RECUPERAR ALMACENAMIENTO DEL LOCALSTORAGE            =
	=================================================================*/

	if(localStorage.getItem("listaProductos") == null){

		listaCarrito = [];

	}else{

		var listaProductos = JSON.parse(localStorage.getItem("listaProductos"));

		for (var i = 0; i < listaProductos.length; i++) {
			
			if(listaProductos[i]["idProducto"] == idProducto && listaProductos[i]["tipo"] == "virtual"){

				/* MOSTRAR ALERTA DE QUE EL PRODUCTO YA EXISTE EN EL CARRITO */

				swal({
								
					title: "El producto ya esta agregado al carrito de compras",
					text: "",
					type: "warning",
					showCancelButton: false,
					confirmButtonColor: "#DD6B55",
					cancelButtonText: "Volver",
					closeOnConfirm: false

			   		});

					return;

			}

		}

		listaCarrito.concat(localStorage.getItem("listaProductos"));

	}

			listaCarrito.push({"idProducto":idProducto, 
							   "imagen": imagen,
							   "titulo": titulo,
							   "precio": precio,
						       "tipo": tipo,
							   "peso": peso,
							   "cantidad": "1"});

		/*=========================================================================================================
		=            JSON STRINGIFY CONVIERTE EL ARRAY EN STRING PORQUE LOCAL STORAGE NO RECIBE ARRAYS            =
		=========================================================================================================*/

		localStorage.setItem("listaProductos", JSON.stringify(listaCarrito));

		/*===========================================
		=            ACTUALIZAR LA CESTA            =
		===========================================*/

		var cantidadCesta = Number($(".cantidadCesta").html()) + 1;
		var sumaCesta = Number($(".sumaCesta").html()) + Number(precio);

		$(".cantidadCesta").html(cantidadCesta);
		$(".sumaCesta").html(sumaCesta);

		localStorage.setItem("cantidadCesta", cantidadCesta);
		localStorage.setItem("sumaCesta", sumaCesta);
		

		/* MOSTRAR ALERTA DE QUE EL PRODUCTO YA FUE AGREGADO */

		swal({
								
				title: "",
				text: "Se ha agregado un nuevo producto al carrito de compras",
				type: "success",
				showCancelButton: true,
				confirmButtonColor: "#DD6B55",
				cancelButtonText: "Continuar Comprando",
				confirmButtonText: "Ir a mi carrito de compras",
				closeOnConfirm: false
			   },
			   	function(isConfirm){

			   		if(isConfirm){

			   			window.location = rutaOculta+"carrito-de-compras";

			   		}
			});
	}
	
})

/*====================================================
=            QUITAR PRODUCTOS DEL CARRITO            =
====================================================*/

$(document).on("click", ".quitarItemCarrito", function(){

	$(this).parent().parent().parent().remove();

	var idProducto = $(".cuerpoCarrito button");
	var imagen = $(".cuerpoCarrito img");
	var titulo = $(".cuerpoCarrito .tituloCarritoCompra");
	var precio = $(".cuerpoCarrito .precioCarritoCompra span");
	var cantidad = $(".cuerpoCarrito .cantidadItem");

	/*==============================================================================
	=  SI AUN QUEDAN PRODUCTOS VOLVERLOS A AGREGAR AL CARRITO(LOCALSTORAGE)    =
	==============================================================================*/

	listaCarrito = [];

	if(idProducto.length != 0){

		for(var i = 0; i < idProducto.length; i++){

			var idProductoArray = $(idProducto[i]).attr("idProducto");
			var imagenArray = $(imagen[i]).attr("src");
			var tituloArray = $(titulo[i]).html();
			var precioArray = $(precio[i]).html();
			var pesoArray = $(idProducto[i]).attr("peso");
			var tipoArray = $(cantidad[i]).attr("tipo");
			var cantidadArray = $(cantidad[i]).val();

			listaCarrito.push({"idProducto":idProductoArray,
							   "imagen":imagenArray,
							   "titulo":tituloArray,
							   "precio":precioArray,
						       "tipo":tipoArray,
					           "peso":pesoArray,
					           "cantidad":cantidadArray});
			
		}

		localStorage.setItem("listaProductos", JSON.stringify(listaCarrito));

		sumaSubtotales();
		cestaCarrito(listaCarrito.length);


	}else{

		/*======================================================================
		=            SI YA NO QUEDAN PRODUCTOS HAY QUE REMOVER TODO            =
		======================================================================*/
		
		localStorage.removeItem("listaProductos");
		localStorage.setItem("cantidadCesta", "0");
		localStorage.setItem("sumaCesta", "0");

		$(".cantidadCesta").html("0");
		$(".sumaCesta").html("0");

		$(".cuerpoCarrito").html('<div class="well text-center backColor">Aun no hay productos en el carrito de compras.</div>');
		$(".sumaCarrito").hide();
		$(".cabeceraCheckout").hide();

	}
	
})

/*=======================================================================
/*=======================================================================
/*=======================================================================
/*=======================================================================
/*=======================================================================
/*=======================================================================
=            GENERAR SUBTOTAL DESPUES DE CAMBIAR DE CANTIDAD            =
=======================================================================*/

$(document).on("change", ".cantidadItem", function(){

	var cantidad = $(this).val();
	var precio = $(this).attr("precio");
	var idProducto = $(this).attr("idProducto");
	var item = $(this).attr("item");

	$(".subTotal"+item).html('<strong>USD $<span>'+(cantidad*precio)+'</span></strong>');

	/* ACTUALIZAR LA CANTIDAD EN EL LOCALSTORAGE */
	
	var idProducto = $(".cuerpoCarrito button");
	var imagen = $(".cuerpoCarrito img");
	var titulo = $(".cuerpoCarrito .tituloCarritoCompra");
	var precio = $(".cuerpoCarrito .precioCarritoCompra span");
	var cantidad = $(".cuerpoCarrito .cantidadItem");

	listaCarrito = [];

	for(var i = 0; i < idProducto.length; i++){

			var idProductoArray = $(idProducto[i]).attr("idProducto");
			var imagenArray = $(imagen[i]).attr("src");
			var tituloArray = $(titulo[i]).html();
			var precioArray = $(precio[i]).html();
			var pesoArray = $(idProducto[i]).attr("peso");
			var tipoArray = $(cantidad[i]).attr("tipo");
			var cantidadArray = $(cantidad[i]).val();

			listaCarrito.push({"idProducto":idProductoArray,
							   "imagen":imagenArray,
							   "titulo":tituloArray,
							   "precio":precioArray,
						       "tipo":tipoArray,
					           "peso":pesoArray,
					           "cantidad":cantidadArray});
			
		}

		localStorage.setItem("listaProductos", JSON.stringify(listaCarrito));

		sumaSubtotales();
		cestaCarrito(listaCarrito.length);
})

/*=======================================================================
/*=======================================================================
/*=======================================================================
/*=======================================================================
/*=======================================================================
/*=======================================================================
=                    SUMA DE TODOS LOS SUBTOTALES                       =
=======================================================================*/

function sumaSubtotales(){

	var subTotales = $(".subTotales span");
	var arraySumaSubTotales = [];
	
	for (var i = 0; i < subTotales.length; i++) {
		
			var subTotalesArray = $(subTotales[i]).html();
			arraySumaSubTotales.push(Number(subTotalesArray));
			
		}

		function sumaArraySubtotales(total, numero){

			return total + numero;

	}

	var sumaTotal = arraySumaSubTotales.reduce(sumaArraySubtotales);

	$(".sumaSubTotal").html('<strong>USD $<span>'+(sumaTotal).toFixed(2)+'</span></strong>');

	$(".sumaCesta").html((sumaTotal).toFixed(2));

	localStorage.setItem("sumaCesta", (sumaTotal).toFixed(2));

}

/*=======================================================================
/*=======================================================================
/*=======================================================================
/*=======================================================================
/*=======================================================================
/*=======================================================================
=                 ACTUALIZAR CESTA AL CAMBIAR CANTIDAD                  =
=======================================================================*/

function cestaCarrito(cantidadProductos){

	/*================================================================
	=            PREGUNTAR SI HAY PRODUCTOS EN EL CARRITO            =
	================================================================*/
	
	if(cantidadProductos != 0){
		
		var cantidadItem = $(".cuerpoCarrito .cantidadItem");
		var arraySumaCantidades = [];

		for (var i = 0; i < cantidadItem.length; i++) {
		
			var cantidadItemArray = $(cantidadItem[i]).val();
			arraySumaCantidades.push(Number(cantidadItemArray));
			
		}


		function sumaArrayCantidades(total, numero){

			return total + numero;

		}

		var sumaTotalCantidades = arraySumaCantidades.reduce(sumaArrayCantidades);
		
		$(".cantidadCesta").html(sumaTotalCantidades);
		localStorage.getItem("cantidadCesta", sumaTotalCantidades);

	}
}

/*=========================================*/
/*=========================================*/
/*=========================================*/
/*=========================================*/
/*=========================================*/
/*=========================================*/
/*=========================================*/
/* Checkout          
/*=========================================*/

$("#btnCheckout").click(function(){

	$(".listaProductos table.tablaProductos tbody").html("");

	var idUsuario = $(this).attr("idUsuario");
	var peso = $(".cuerpoCarrito button, .comprarAhora button");
	var titulo = $(".cuerpoCarrito .tituloCarritoCompra,.comprarAhora .tituloCarritoCompra");
	var cantidad = $(".cuerpoCarrito .cantidadItem, .comprarAhora .cantidadItem");
	var subtotal = $(".cuerpoCarrito .subTotales span, .comprarAhora .subTotales span");
	var tipoArray = [];
	var cantidadPeso = [];

	/*=====================================
	=            Suma SubTotal            =
	=====================================*/

	var sumaSubTotal = $(".sumaSubTotal span");
	

	$(".valorSubTotal").html($(sumaSubTotal).html());
	$(".valorSubTotal").attr("valor", $(sumaSubTotal).html());

	/*=========================================
	=            Tasas de impuesto            =
	=========================================*/

	var impuestoTotal = ($(".valorSubTotal").html() * $("#tasaImpuesto").val())/100;
	
	$(".valorTotalImpuesto").html(impuestoTotal.toFixed(2));
	$(".valorTotalImpuesto").attr("valor", impuestoTotal.toFixed(2));
	
	sumaTotalDefinitivo();

	/*=======================================
	=            Variables array            =
	=======================================*/
	

	for (var i = 0; i < peso.length; i++) {

		var pesoArray = $(peso[i]).attr("peso");
		
		var tituloArray = $(titulo[i]).html();
		
		var cantidadArray = $(cantidad[i]).val();
		
		var subtotalArray = $(subtotal[i]).html();

		/*=============================================================================
		=            Evaluar el peso de acuerdo a la cantidad de productos            =
		=============================================================================*/

		cantidadPeso[i] = pesoArray * cantidadArray;
		
		function sumaArrayPeso(total, numero){

			return total + numero;

		}

		var sumaTotalPeso = cantidadPeso.reduce(sumaArrayPeso);

		/*===============================================================
		=            Mostrar Productos Definitivos a Comprar            =
		===============================================================*/

		$(".listaProductos table.tablaProductos tbody").append('<tr>'+
															   '<td class="valorTitulo">'+tituloArray+'</td>'+
															   '<td class="valorCantidad">'+cantidadArray+'</td>'+
															   '<td>$ <span class="valorItem" value="'+subtotalArray+'">'+subtotalArray+'</span></td>'+
															   '</tr>');
		
		/*=======================================================================================
		=            Seleccionar pais de envio si el producto es fisico y no virtual            =
		=======================================================================================*/

		
		
		tipoArray.push($(cantidad[i]).attr("tipo"));

		function checkTipo(tipo){

			return tipo == "fisico";

		}

	}

		/*=================================================
		=            Existen productos fisicos            =
		=================================================*/

		if(tipoArray.find(checkTipo) == "fisico"){

			$(".seleccionePais").html('<select class="form-control" name="seleccionarPais" id="seleccionarPais" required>'+
							
						'<option value="">Seleccione el Pais</option>'+

					 '</select>');

			$(".formEnvio").show();

			$(".btnPagar").attr("tipo", "fisico");

			$.ajax({

				url: rutaOculta+"vistas/js/plugins/countries.json",
				type: "GET",
				cache: false,
				contentType: false,
				processData: false,
				dataType: "json",
				success: function(respuesta){
					
					respuesta.forEach(seleccionarPais);

					function seleccionarPais(item, index){

						var pais = item.name;
						var codPais = item.code;

						$("#seleccionarPais").append('<option value="'+codPais+'">'+pais+'</option>')

					}

				}

			})

			/*==========================================================================
			=            Evaluar tasas de impuesto si el producto es fisico            =
			==========================================================================*/

			$("#seleccionarPais").change(function(){

				$(".alert").remove();

				$("#cambiarDivisa").val("USD");

				$(".cambioDivisa").html("USD");

				$(".valorSubTotal").html((1 * Number($(".valorSubTotal").attr("valor"))).toFixed(2));
				$(".valorTotalEnvio").html((1 * Number($(".valorTotalEnvio").attr("valor"))).toFixed(2));
				$(".valorTotalImpuesto").html((1 * Number($(".valorTotalImpuesto").attr("valor"))).toFixed(2));
				$(".valorTotalCompra").html((1 * Number($(".valorTotalCompra").attr("valor"))).toFixed(2));

				var valorItem = $(".valorItem");

				for (var i = 0; i < valorItem.length; i++) {
				 	
					$(valorItem[i]).html((1 * Number($(valorItem[i]).attr("value"))).toFixed(2));

				 } 

				var pais = $(this).val();
				var tasaPais = $("#tasaPais").val();

				if(pais == tasaPais){

					var resultadoPeso = sumaTotalPeso * $("#envioNacional").val();

					if(resultadoPeso < $("#tasaMinimaNal").val()){

						$(".valorTotalEnvio").html($("#tasaMinimaNal").val());
						$(".valorTotalEnvio").attr("valor", $("#tasaMinimaNal").val());

					}else{

						$(".valorTotalEnvio").html(resultadoPeso);
						$(".valorTotalEnvio").attr("valor", resultadoPeso);

					}

				}else{ 

					var resultadoPeso = sumaTotalPeso * $("#envioInternacional").val();

					if(resultadoPeso < $("#tasaMinimaInt").val()){

						$(".valorTotalEnvio").html($("#tasaMinimaInt").val());
						$(".valorTotalEnvio").attr("valor", $("#tasaMinimaInt").val());

					}else{

						$(".valorTotalEnvio").html(resultadoPeso);
						$(".valorTotalEnvio").attr("valor", resultadoPeso);

					}

				}

				/*=======================================================
				=            Retornar cambio de divisa a USD            =
				=======================================================*/
				
				$(".cambioDivisa").html("USD");

				$(".valorSubTotal").html((1 * Number($(".valorSubTotal").attr("valor"))).toFixed(2));
				$(".valorTotalEnvio").html((1 * Number($(".valorTotalEnvio").attr("valor"))).toFixed(2));
				$(".valorTotalImpuesto").html((1 * Number($(".valorTotalImpuesto").attr("valor"))).toFixed(2));
				$(".valorTotalCompra").html((1 * Number($(".valorTotalCompra").attr("valor"))).toFixed(2));

				var valorItem = $(".valorItem");

				for (var i = 0; i < valorItem.length; i++) {
				 	
					$(valorItem[i]).html((1 * Number($(valorItem[i]).attr("value"))).toFixed(2));

				 }

				sumaTotalDefinitivo();

			})
			

		}else{

			$(".btnPagar").attr("tipo", "virtual");

		}
})

/*============================================================================
=            Funcion de la suma total definitiva de los productos            =
============================================================================*/

function sumaTotalDefinitivo(){

	var sumaTotalTasas = Number($(".valorSubTotal").html()) + 
						 Number($(".valorTotalEnvio").html())+ 
						 Number($(".valorTotalImpuesto").html());

	$(".valorTotalCompra").html(sumaTotalTasas.toFixed(2));
	$(".valorTotalCompra").attr("valor", sumaTotalTasas.toFixed(2));

	localStorage.setItem("total", hex_md5($(".valorTotalCompra").html()));
	

}

/*============================================================
=            Metodo de pago para cambio de divisa            =
============================================================*/

var metodoPago = "paypal";
divisas(metodoPago);

$("input[name='pago']").change(function(){

	var metodoPago = $(this).val();

	divisas(metodoPago);

})

/*========================================================
=            Funcion para el cambio de divisa            =
========================================================*/

function divisas(metodoPago){

	$("#cambiarDivisa").html("");

	if(metodoPago == "paypal"){

		$("#cambiarDivisa").append('<option value="USD">USD</option>'+
								   '<option value="EUR">EUR</option>'+
								   '<option value="GBP">GBP</option>'+
								   '<option value="MXN">MXN</option>'+
								   '<option value="JPY">JPY</option>'+
								   '<option value="CAD">CAD</option>'+
								   '<option value="BRL">BRL</option>');

	}else if(metodoPago == "payu"){

		$("#cambiarDivisa").append('<option value="USD">USD</option>'+
								   '<option value="PEN">PEN</option>'+
								   '<option value="COP">COP</option>'+
								   '<option value="MXN">MXN</option>'+
								   '<option value="CLP">CLP</option>'+
								   '<option value="ARS">ARS</option>'+
								   '<option value="BRL">BRL</option>');

	}

}

/*=========================================
=            Cambiar de Divisa            =
=========================================*/

var divisaBase = "USD";

$("#cambiarDivisa").change(function(){

	$(".alert").remove();

	if($("#seleccionarPais").val() == ""){

		$("#cambiarDivisa").after('<div class="alert alert-warning">No ha seleccionado el pais de envio</div>');

		return;

	}

	var divisa = $(this).val();

	$.ajax({

		url: "https://free.currencyconverterapi.com/api/v3/convert?q="+divisaBase+"_"+divisa+"&compact=y&apiKey=0e21bb69bbac95343b71",
		type: "GET",
		cache: false,
		contentType: false,
		processData: false,
		dataType:"jsonp",
		success:function(respuesta){

			var conversion = respuesta["USD_"+divisa]["val"];

			if(divisa == "USD"){

				conversion = 1;

			}

			$(".cambioDivisa").html(divisa);

			$(".valorSubTotal").html((Number(conversion) * Number($(".valorSubTotal").attr("valor"))).toFixed(2));
			$(".valorTotalEnvio").html((Number(conversion) * Number($(".valorTotalEnvio").attr("valor"))).toFixed(2));
			$(".valorTotalImpuesto").html((Number(conversion) * Number($(".valorTotalImpuesto").attr("valor"))).toFixed(2));
			$(".valorTotalCompra").html((Number(conversion) * Number($(".valorTotalCompra").attr("valor"))).toFixed(2));

			var valorItem = $(".valorItem");

			localStorage.setItem("total", hex_md5($(".valorTotalCompra").html()));

			for (var i = 0; i < valorItem.length; i++) {
			 	
				$(valorItem[i]).html((Number(conversion) * Number($(valorItem[i]).attr("value"))).toFixed(2));

			 } 

			
		}

	})

})


/*===================================
=        Boton Pagar PayPal         =
===================================*/

$(".btnPagar").click(function(){

	var tipo = $(this).attr("tipo");

	if(tipo == "fisico" && $("#seleccionarPais").val() == ""){

		$(".btnPagar").after('<div class="alert alert-warning">No ha seleccionado el pais de envio</div>');

		return;

	}

	var divisa = $("#cambiarDivisa").val();
	var total = $(".valorTotalCompra").html();
	var totalEncriptado = localStorage.getItem("total");
	var impuesto = $(".valorTotalImpuesto").html();
	var tasasEnvio = $(".valorTotalEnvio").html();
	var subTotal = $(".valorSubTotal").html();
	var titulo = $(".valorTitulo");
	var cantidad = $(".valorCantidad");
	var item = $(".valorItem"); 
	var idProducto = $(".cuerpoCarrito button, .comprarAhora button");

	var tituloArray = [];
	var cantidadArray = [];
	var valorItemArray = [];
	var idProductoArray = [];

	for (var i = 0; i < titulo.length; i++) {
		
		 tituloArray[i] = $(titulo[i]).html();
		 
		 cantidadArray[i] = $(cantidad[i]).html();
		 
		 valorItemArray[i] = $(item[i]).html();
		 
		 idProductoArray[i] = $(idProducto[i]).attr("idProducto");
		 
	}

	var datos = new FormData;
	datos.append("divisa", divisa);
	datos.append("total", total);
	datos.append("totalEncriptado", totalEncriptado);
	datos.append("impuesto", impuesto);
	datos.append("tasasEnvio", tasasEnvio);
	datos.append("subTotal", subTotal);
	datos.append("tituloArray", tituloArray);
	datos.append("cantidadArray", cantidadArray);
	datos.append("valorItemArray", valorItemArray);
	datos.append("idProductoArray", idProductoArray);

	$.ajax({

		url: rutaOculta+"ajax/carrito.ajax.php",
		method: "POST",
		data: datos,
		cache: false,
		contentType: false,
		processData: false,
		success:function(respuesta){

			window.location = respuesta;

		}

	})

})


/*===================================
=       Agregar Productos Gratis    =
===================================*/

$(".agregarGratis").click(function(){

	var idProducto = $(this).attr("idProducto");
	var idUsuario = $(this).attr("idUsuario");
	var tipo = $(this).attr("tipo");
	var titulo = $(this).attr("titulo");
	var agregarGratis = false;

	/*====================================================================
	=            Verificar que no tenga el producto adquirido            =
	====================================================================*/

	var datos = new FormData();

	datos.append("idUsuario", idUsuario);
	datos.append("idProducto", idProducto);

	$.ajax({

		url:rutaOculta+"ajax/carrito.ajax.php",
		method: "POST",
		data: datos,
		cache: false,
		contentType: false,
		processData: false,
		success:function(respuesta){
			
			if(respuesta != "false"){

				swal({

					title: "Usted ya adquirio este producto!",
					text: "",
					type: "warning",
					showCancelButton: false,
					confirmButtonColor: "#DD6B55",
					confirmButtonText: "Regresar",
					closeOnConfirm: false
				})

			}else{

				if(tipo == "virtual"){

					agregarGratis = true;

				}else{

					var seleccionarDetalle = $(".seleccionarDetalle");

					for (var i = 0; i < seleccionarDetalle.length; i++) {
						
						if($(seleccionarDetalle[i]).val() == ""){

							swal({
											
								title: "Debe seleccionar talla y color",
								text: "Para agregar un producto a su carrito de compras, debe elegir talla y color",
								type: "warning",
								confirmButtonText: "Cerrar",
								closeOnConfirm: false

							});

						}else{

							titulo = titulo + "-" + $(seleccionarDetalle[i]).val();

							agregarGratis = true;
							
						}

					}

				}

				if(agregarGratis){

					window.location = rutaOculta+"index.php?ruta=finalizar-compra&gratis=true&producto="+idProducto+"&titulo="+titulo;

				}
			}

		}

	})
})



