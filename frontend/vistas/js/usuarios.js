/* CAPTURA DE RUTA */

var rutaActual = location.href;

$(".btnIngreso, .facebook, .google").click(function(){

	localStorage.setItem("rutaActual", rutaActual);

});

/* FORMATEAR LOS INPUT */

$("input").focus(function(){

	$(".alert").remove();

});


/* VALIDAR EMAIL REPETIDO */

var validarEmailRepetido = false;

$("#regEmail").change(function(){

	var email = $("#regEmail").val();

	var datos = new FormData();
	datos.append("validarEmail", email);

	$.ajax({

		url: rutaOculta+"ajax/usuarios.ajax.php",
		method: "POST",
		data: datos,
		cache: false,
		contentType: false,
		processData: false,
		success: function(respuesta){

			$(".alert").remove();

			if(respuesta == "false"){

				$(".alert").remove();
				validarEmailRepetido = false;

			}else{

				var modo = JSON.parse(respuesta).modo;

				if(modo == "directo"){

					modo = "esta pagina"

				}

				$("#regEmail").parent().before('<div class="alert alert-danger">ERROR: El correo electronico ya existe, fue registrado a traves de '+modo+' Por favor ingrese otro diferente</div>');

				validarEmailRepetido = true;
			}

		}

	})

})

/* VALIDAR EL REGISTRO DE USUARIO */

function registroUsuario(){

	/* VALIDAR EL NOMBRE */
	

	var nombre = $("#regUsuario").val();

	if(nombre != ""){

		var expresion = /^[a-zA-ZñÑáéíóúÁÉÍÓÚ ]*$/;

		if(!expresion.test(nombre)){

			$("#regUsuario").parent().before('<div class="alert alert-warning"><strong>Error: No se permite numero ni caracteres especiales</strong></div>')

			return false;
		}

	}else{

			$("#regUsuario").parent().before('<div class="alert alert-warning"><strong>Atencion: Este campo es obligatorio</strong></div>')

			return false;

	}

	/* VALIDAR EL EMAIL */

	var email = $("#regEmail").val();

	if(email != ""){

		var expresion = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,4})+$/;

		if(!expresion.test(email)){

			$("#regEmail").parent().before('<div class="alert alert-warning"><strong>ERROR:</strong> Escriba correctamente el correo electrónico</div>')

			return false;

		}

		if(validarEmailRepetido == true){

			$(".alert").remove();

			$("#regEmail").parent().before('<div class="alert alert-danger">ERROR: El correo electronico ya existe, Por favor ingrese otro diferente</div>');

			return false;
		}

	}else{

		$("#regEmail").parent().before('<div class="alert alert-danger"><strong>ERROR:</strong>Este campo es obligatorio </div>')

		return false;

	}

	/* VALIDAR EL PASSWORD */

	var password = $("#regPassword").val();

	if(password != ""){

		var expresion = /^[a-zA-Z0-9]*$/;;

		if(!expresion.test(password)){

			$("#regPassword").parent().before('<div class="alert alert-warning"><strong>ERROR:</strong> El password solo debe llevar letras y numeros</div>')

			return false;

		}
	}else{

		$("#regPassword").parent().before('<div class="alert alert-danger"><strong>ERROR:</strong>Este campo es obligatorio </div>')

		return false;

	}

	/* VALIDAR POLITICAS DE PRIVACIDAD */

	var politicas = $("#regPoliticas:checked").val();

	if(politicas != "on"){

		$("#regPoliticas").parent().before('<div class="alert alert-warning"><strong>Atencion: Debe aceptar nuestras condiciones de uso y politicas de privacidad</strong></div>')

		return false;
	}

	return true;
}

	/* CAMBIAR FOTO */

	$("#btnCambiarFoto").click(function(){

		$("#imgPerfil").toggle();
		$("#btnVolverFoto").show();
		$("#btnCambiarFoto").hide();
		$("#subirImagen").toggle();

	})

	$("#btnVolverFoto").on("click", function(){

		$("#imgPerfil").toggle();
		$("#btnVolverFoto").hide();
		$("#btnCambiarFoto").show();
		$("#subirImagen").toggle();

	})

	$("#datosImagen").change(function(){

		var imagen = this.files[0];
		console.log("imagen", imagen);

		/* VALIDAMOS EL FORMATO DE LA IMAGEN */
		
		if(imagen["type"] != "image/jpeg" && imagen["type"] != "image/png"){

			$("#datosImagen").val("");

			swal({
				
				title: "Error al subir la imagen",
				text: "La imagen debe ser subida en formato JPG o PNG!!",
				type: "error",
				confirmButtonText: "Cerrar",
				closeOnConfirm: false
			},
				function(isConfirm){
					
					if(isConfirm){
						window.location = rutaOculta+"perfil";
					}

			});

		}else if(Number(imagen["size"]) > 2000000){

			$("#datosImagen").val("");

			swal({
				
				title: "Error al subir la imagen",
				text: "La imagen no debe superar los 2 megabytes",
				type: "error",
				confirmButtonText: "Cerrar",
				closeOnConfirm: false
			},
				function(isConfirm){
					
					if(isConfirm){
						window.location = rutaOculta+"perfil";
					}

			});

		}else{

			var datosImagen = new FileReader;
			datosImagen.readAsDataURL(imagen);

			$(datosImagen).on("load", function(event){

				var rutaImagen = event.target.result;
				$(".previsualizar").attr("src", rutaImagen);

			})
		}

	})

	/* Comentarios */

	$(".calificarProducto").click(function(){

		var idComentario = $(this).attr("idComentario");

		$("#idComentario").val(idComentario);

	})

	/* Comentarios Cambio de estrellas*/
	$("input[name = 'puntaje']").change(function(){

		var puntaje = $(this).val()

		switch(puntaje){

			case "0.5":
			$("#estrellas").html('<i class="fa fa-star-half-o text-success" aria-hidden="true"></i> '+
								  '<i class="fa fa-star-o text-success" aria-hidden="true"></i> '+
								  '<i class="fa fa-star-o text-success" aria-hidden="true"></i> '+
								  '<i class="fa fa-star-o text-success" aria-hidden="true"></i> '+
								  '<i class="fa fa-star-o text-success" aria-hidden="true"></i> ');
			break;

			case "1.0":
			$("#estrellas").html('<i class="fa fa-star text-success" aria-hidden="true"></i> '+
								 '<i class="fa fa-star-o text-success" aria-hidden="true"></i> '+
								 '<i class="fa fa-star-o text-success" aria-hidden="true"></i> '+
								 '<i class="fa fa-star-o text-success" aria-hidden="true"></i> '+
								 '<i class="fa fa-star-o text-success" aria-hidden="true"></i> ');
			break;

			case "1.5":
			$("#estrellas").html('<i class="fa fa-star text-success" aria-hidden="true"></i> '+
								 '<i class="fa fa-star-half-o text-success" aria-hidden="true"></i> '+
								 '<i class="fa fa-star-o text-success" aria-hidden="true"></i> '+
								 '<i class="fa fa-star-o text-success" aria-hidden="true"></i> '+
								 '<i class="fa fa-star-o text-success" aria-hidden="true"></i> ');
			break;

			case "2.0":
			$("#estrellas").html('<i class="fa fa-star text-success" aria-hidden="true"></i> '+
								 '<i class="fa fa-star text-success" aria-hidden="true"></i> '+
								 '<i class="fa fa-star-o text-success" aria-hidden="true"></i> '+
								 '<i class="fa fa-star-o text-success" aria-hidden="true"></i> '+
								 '<i class="fa fa-star-o text-success" aria-hidden="true"></i> ');
			break;

			case "2.5":
			$("#estrellas").html('<i class="fa fa-star text-success" aria-hidden="true"></i> '+
								 '<i class="fa fa-star text-success" aria-hidden="true"></i> '+
								 '<i class="fa fa-star-half-o text-success" aria-hidden="true"></i> '+
								 '<i class="fa fa-star-o text-success" aria-hidden="true"></i> '+
								 '<i class="fa fa-star-o text-success" aria-hidden="true"></i> ');
			break;

			case "3.0":
			$("#estrellas").html('<i class="fa fa-star text-success" aria-hidden="true"></i> '+
								 '<i class="fa fa-star text-success" aria-hidden="true"></i> '+
								 '<i class="fa fa-star text-success" aria-hidden="true"></i> '+
								 '<i class="fa fa-star-o text-success" aria-hidden="true"></i> '+
								 '<i class="fa fa-star-o text-success" aria-hidden="true"></i> ');
			break;

			case "3.5":
			$("#estrellas").html('<i class="fa fa-star text-success" aria-hidden="true"></i> '+
								 '<i class="fa fa-star text-success" aria-hidden="true"></i> '+
								 '<i class="fa fa-star text-success" aria-hidden="true"></i> '+
								 '<i class="fa fa-star-half-o text-success" aria-hidden="true"></i> '+
								 '<i class="fa fa-star-o text-success" aria-hidden="true"></i> ');
			break;

			case "4.0":
			$("#estrellas").html('<i class="fa fa-star text-success" aria-hidden="true"></i> '+
								 '<i class="fa fa-star text-success" aria-hidden="true"></i> '+
								 '<i class="fa fa-star text-success" aria-hidden="true"></i> '+
								 '<i class="fa fa-star text-success" aria-hidden="true"></i> '+
								 '<i class="fa fa-star-o text-success" aria-hidden="true"></i> ');
			break;

			case "4.5":
			$("#estrellas").html('<i class="fa fa-star text-success" aria-hidden="true"></i> '+
								 '<i class="fa fa-star text-success" aria-hidden="true"></i> '+
								 '<i class="fa fa-star text-success" aria-hidden="true"></i> '+
								 '<i class="fa fa-star text-success" aria-hidden="true"></i> '+
								 '<i class="fa fa-star-half-o text-success" aria-hidden="true"></i> ');
			break;

			case "5.0":
			$("#estrellas").html('<i class="fa fa-star text-success" aria-hidden="true"></i> '+
								 '<i class="fa fa-star text-success" aria-hidden="true"></i> '+
								 '<i class="fa fa-star text-success" aria-hidden="true"></i> '+
								 '<i class="fa fa-star text-success" aria-hidden="true"></i> '+
								 '<i class="fa fa-star text-success" aria-hidden="true"></i> ');
			break;

		}

	})
	
	/* Validar Comentario */

	function validarComentario(){

		var comentario = $("#comentario").val();

		if(comentario != ""){

			var expresion = /^[,\\.\\a-zA-Z0-9ñÑáéíóúÁÉÍÓÚ ]*$/;

			if(!expresion.test(comentario)){

				$(".alert").remove();
				$("#comentario").parent().before('<div class="alert alert-danger"><strong>Error: </strong>No se permiten caracteres especiales, por ejemplo: ¡!¿?$%&/[]*</div>');

				return false;

			}
		}else{

			$(".alert").remove();
			$("#comentario").parent().before('<div class="alert alert-danger"><strong>Error: </strong>Campo de comentarios es obligatorio!!</div>');

				return false;


		}

		return true;

	}

	/* Lista de Deseos */

	$(".deseos").click(function(){

		var idProducto = $(this).attr("idProducto");

		var idUsuario = localStorage.getItem("usuario");

		if(idUsuario == null){

			swal({
								
					title: "Debe Ingresar al sistema",
					text: "Para agregar un producto a su lista de deseos debe iniciar sesion en el sistema",
					type: "warning",
					confirmButtonText: "Cerrar",
					closeOnConfirm: false
				},
					function(isConfirm){
						
						if(isConfirm){
							window.location = rutaOculta;
						}

				});

		}else{

			$(this).addClass("btn-danger");

			var datos = new FormData();
			datos.append("idUsuario", idUsuario);
			datos.append("idProducto", idProducto);

			$.ajax({

				url:rutaOculta+"ajax/usuarios.ajax.php",
				method: "POST",
				data: datos,
				cache: false,
				contentType: false,
				processData: false,
				success:function(respuesta){
				}

			})

		}

	})

	/* BORRAR PRODUCTO DE LA LISTA DE DESEO */

	$(".quitarDeseo").click(function(){	

	var idDeseo = $(this).attr("idDeseo");

	$(this).parent().parent().parent().remove();

	var datos = new FormData();
	datos.append("idDeseo", idDeseo);

	$.ajax({
			url:rutaOculta+"ajax/usuarios.ajax.php",
			method:"POST",
			data: datos,
			cache: false,
			contentType: false,
			processData: false,
			success:function(respuesta){
				console.log("respuesta", respuesta);
			
			}

		});

	})

	/* 	Eliminar Usuario */

	$("#eliminarUsuario").click(function(){

		var id = $("#idUsuario").val();

		if($("#modoUsuario").val() == "directo"){

			if($("#fotoUsuario").val() != ""){

					var foto = $("#fotoUsuario").val();

			}

		}

		swal({
				
				title: "Esta usted seguro de eliminar su cuenta?",
				text: "Si borra esta cuenta ya no se podra recuperar",
				type: "warning",
				showCancelButton: true,
				confirmButtonColor: "#DD6B55",
				confirmButtonText: "Si, borrar cuenta",
				closeOnConfirm: false
			},
				function(isConfirm){
					
					if(isConfirm){
						window.location = "index.php?ruta=perfil&id="+id+"&foto="+foto;
					}

			});

	})

	/*=========================================================
	=            Validacion formulario Contactenos            =
	=========================================================*/
	
	function validarContactenos(){


		var nombre = $("#nombreContactenos").val();
		var email = $("#emailContactenos").val();
		var mensaje = $("#mensajeContactenos").val();

		/*=============================================
		=            Validacion del Nombre            =
		=============================================*/
		

		if(nombre == ""){

			$("#nombreContactenos").before('<h6 class="alert alert-danger">Escriba porfavor el Nombre</h6>')

			return false;

		}else{

			var expresion = /^[a-zA-ZñÑáéíóúÁÉÍÓÚ ]*$/;

			if(!expresion.test(nombre)){

				$("#nombreContactenos").before('<h6 class="alert alert-danger">Escriba porfavor solo letras sin caracteres especiales</h6>')

				return false;

			}

		}

		/*============================================
		=            Validacion del Email            =
		============================================*/
		

		if(email == ""){

			$("#emailContactenos").before('<h6 class="alert alert-danger">Escriba porfavor el Email</h6>')

			return false;

		}else{

			var expresion = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,4})+$/;

			if(!expresion.test(email)){

				$("#emailContactenos").before('<h6 class="alert alert-danger">Escriba correctamente el Correo Electronico</h6>')

				return false;

			}

		}

		/*==============================================
		=            Validacion del Mensaje            =
		==============================================*/

		if(mensaje == ""){

			$("#mensajeContactenos").before('<h6 class="alert alert-danger">Escriba porfavor el Email</h6>')

			return false;

		}else{

			var expresion = /^[,\\.\\a-zA-Z0-9ñÑáéíóúÁÉÍÓÚ ]*$/;

			if(!expresion.test(mensaje)){

				$("#mensajeContactenos").before('<h6 class="alert alert-danger">Escriba porfavor solo letras sin caracteres especiales</h6>')

				return false;

			}

		}

		return true;

	}

	




