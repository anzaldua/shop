
/* PLANTILLA */

var rutaOculta = $("#rutaOculta").val();

//Herramienta TOOLTIP

$('[data-toggle="tooltip"]').tooltip();

$.ajax({

	url:rutaOculta+"ajax/plantilla.ajax.php",
	success:function(respuesta){

		var colorFondo = JSON.parse(respuesta).colorFondo;
		var colorTexto = JSON.parse(respuesta).colorTexto;
		var barraSuperior = JSON.parse(respuesta).barraSuperior;
		var textoSuperior = JSON.parse(respuesta).textoSuperior;

		$(".backColor, .backColor a").css({"background": colorFondo,"color": colorTexto});

		$(".barraSuperior, .barraSuperior a").css({"background": barraSuperior,"color": textoSuperior});

	}

})

/* VISUALIZAR CUADRICULA O LISTA */

var btnList = $(".btnList");

for (var i = 0; i < btnList.length; i++) {
	
	$("#btnGrid"+i).click(function(){

		var numero = $(this).attr("id").substr(-1);

		$(".list"+numero).hide();
		$(".grid"+numero).show();

		$("#btnGrid"+i).addClass("backColor");
		$("#btnList"+i).removeClass("backColor");

		})

		$("#btnList"+i).click(function(){

		var numero = $(this).attr("id").substr(-1);

		$(".list"+numero).show();
		$(".grid"+numero).hide();

		$("#btnGrid"+i).removeClass("backColor");
		$("#btnList"+i).addClass("backColor");

	})

}

/* EFECTOS CON EL SCROLL */

$(window).scroll(function(){

	var scrollY = window.pageYOffset;

	if(window.matchMedia("(min-width:768px)").matches){

		if($(".banner").html() != null){

			if(scrollY < ($(".banner").offset().top)-300){

			$(".banner img").css({"margin-top":-scrollY/2+"px"})

			}else{

				scrollY = 0;

			}

		}
	
		

	}

})

/* BOTON PARA SUBIR A LA PARTE SUPERIOR DE LA PAGINA */

$.scrollUp({

	scrollText:"",
	scrollSpedd: 2000,
	easingType: "easeOutQuint"

})

/* MIGAS DE PAN BreadCrumb QUITAR GUIONES*/

var pagActiva = $(".paginaActiva").html();

if(pagActiva != null){

	var regPagActiva = pagActiva.replace(/-/, " ");

	$(".paginaActiva").html(regPagActiva);

}

/* ENALCES PAGINACION */

var url = window.location.href;

var indice = url.split("/")

var pagActual = indice[6];

if(isNaN(pagActual)){

	$("#item1").addClass("active")

}else{

	$("#item"+pagActual).addClass("active")
	
}

/* Ofertas */

$(".cerrarOfertas").click(function(){

	$(this).parent().remove();

});

/* Contador de tiempo */

var finOferta = $(".countdown");
var fechaFinOferta = [];

	for (var i = 0; i < finOferta.length; i++) {
		
		fechaFinOferta[i]= $(finOferta[i]).attr("finOferta");

		$(finOferta[i]).dsCountDown({

		endDate: new Date(fechaFinOferta[i]),

		theme: 'black', // Set the theme 'white', 'black', 'red', 'flat', 'custom'

		titleDays: 'Dias', // Set the title of days

		titleHours: 'Horas', // Set the title of hours

		titleMinutes: 'Minutos', // Set the title of minutes

		titleSeconds: 'Segundos'

		});

	}

/* Eventos de Pixel de Facebook */

$(".pixelCategoria").click(function(){

	var titulo = $(this).attr("titulo");

	fbq('track', 'Categoria '+titulo, {

		title: titulo

	})

})

$(".pixelSubCategoria").click(function(){

	var titulo = $(this).attr("titulo");

	fbq('track', 'subCategoria '+titulo, {

		title: titulo

	})

})

$(".pixelOferta").click(function(){

	var titulo = $(this).attr("titulo");

	fbq('track', 'Oferta '+titulo, {

		title: titulo

	})

})







