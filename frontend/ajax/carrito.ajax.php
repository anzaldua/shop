<?php

	require_once "../extensiones/paypal.controlador.php";

	require_once "../controladores/carrito.controlador.php";
	require_once "../modelos/carrito.modelo.php";

	require_once "../controladores/productos.controlador.php";
	require_once "../modelos/productos.modelo.php";

	class AjaxCarrito{

		/*=====================================
		=            Metodo Paypal            =
		=====================================*/

		public $divisa;
		public $total;
		public $totalEncriptado;
		public $impuesto;
		public $tasasEnvio;
		public $subTotal;
		public $tituloArray;
		public $cantidadArray;
		public $valorItemArray;
		public $idProductoArray;
			
		public function ajaxEnviarPaypal(){

			if(md5($this->total) == $this->totalEncriptado){

				$datos = array(
						"divisa"=>$this-> divisa,
						"total"=>$this-> total,
						"impuesto"=>$this-> impuesto,
						"tasasEnvio"=>$this-> tasasEnvio,
						"subTotal"=>$this-> subTotal,
						"tituloArray"=>$this-> tituloArray,
						"cantidadArray"=>$this-> cantidadArray,
						"valorItemArray"=>$this-> valorItemArray,
						"idProductoArray"=>$this-> idProductoArray
						);

				$respuesta = Paypal::mdlPagoPaypal($datos);

				echo $respuesta;
			
			}
		}

		/*=============================================================================
		=            Verificar que no tengo el producto gratuito adquirido            =
		=============================================================================*/

		public $idUsuario;
		public $idProducto;

		public function ajaxVerificarProducto(){

			$datos = array("idUsuario"=>$this->idUsuario,
						   "idProducto"=>$this->idProducto);

			$respuesta = ControladorProductos::ctrVerificarProducto($datos);

			echo json_encode($respuesta);

		}
		
	}

	/*=====================================
	=            Metodo Paypal            =
	=====================================*/

	if(isset($_POST["divisa"])){

		$idProductos = explode(",", $_POST["idProductoArray"]);
		$cantidadProductos = explode(",", $_POST["cantidadArray"]);
		$precioProductos = explode(",", $_POST["valorItemArray"]);

		$item = "id";

		for ($i=0; $i < count($idProductos); $i++) { 

			$valor = $idProductos[$i];
			
			$verificarProductos = ControladorProductos::ctrMostrarInfoProducto($item, $valor);

			$divisa = file_get_contents("https://free.currencyconverterapi.com/api/v3/convert?q=USD_".$_POST["divisa"]."&compact=y&apiKey=0e21bb69bbac95343b71");

			$jsonDivisa = json_decode($divisa, true);

			$conversion = $jsonDivisa["USD_".$_POST["divisa"]]["val"];

			if($verificarProductos["precioOferta"] == 0){

				$precio = number_format($verificarProductos["precio"]*$conversion, 2);

			}else{

				$precio = number_format($verificarProductos["precioOferta"]*$conversion, 2);

			}

			$verificarSubTotal = $cantidadProductos[$i]*$precio;

			if($verificarSubTotal != $precioProductos[$i]){

				echo "carrito-de-compras";

			}

		}

		$paypal = new AjaxCarrito();
		$paypal -> divisa = $_POST["divisa"];
		$paypal -> total = $_POST["total"];
		$paypal -> totalEncriptado = $_POST["totalEncriptado"];
		$paypal -> impuesto = $_POST["impuesto"];
		$paypal -> tasasEnvio = $_POST["tasasEnvio"];
		$paypal -> subTotal = $_POST["subTotal"];
		$paypal -> tituloArray = $_POST["tituloArray"];
		$paypal -> cantidadArray = $_POST["cantidadArray"];
		$paypal -> valorItemArray = $_POST["valorItemArray"];
		$paypal -> idProductoArray = $_POST["idProductoArray"];
		$paypal -> ajaxEnviarPaypal();

	}

	/*=============================================================================
	=            Verificar que no tengo el producto gratuito adquirido            =
	=============================================================================*/

	if(isset($_POST["idProducto"])){

	$producto = new AjaxCarrito();
	$producto -> idUsuario = $_POST["idUsuario"];
	$producto -> idProducto = $_POST["idProducto"];
	$producto -> ajaxVerificarProducto();	

	}
	