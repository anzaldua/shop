<?php

	require_once "../controladores/usuarios.controlador.php";
	require_once "../modelos/usuarios.modelo.php";

	class AjaxUsuarios{

	/*=============================================
	=            VALIDAR EMAIL EXISTENTE          =
	=============================================*/

		public $validarEmail;

		public function ajaxValidarEmail(){

			$datos = $this->validarEmail;

			$item = "email";

			$respuesta = ControladorUsuarios::ctrMostrarUsuario($item, $datos);

			echo json_encode($respuesta);

		}

	/*=============================================
	=            REGISTRO CON FACEBOOK         =
	=============================================*/

		public $email;
		public $nombre;
		public $foto;

		public function ajaxRegistroFacebook(){

			$datos = array("nombre"=>$this->nombre,
							"email"=>$this->email,
							"foto"=>$this->foto,
							"password"=>"null",
							"modo"=>"facebook",
							"verificacion"=>0,
							"emailEncriptado"=>"null");

			$respuesta = ControladorUsuarios::ctrRegistroRedesSociales($datos);

			echo $respuesta;

		}

	/*=============================================
	=            Agregar a lista de Deseos        =
	=============================================*/

		public $idUsuario;
		public $idProducto;

		public function ajaxAgregarDeseo(){

			$datos = array("idUsuario"=>$this->idUsuario,
						   "idProducto"=>$this->idProducto);

			$respuesta = ControladorUsuarios::ctrAgregarDeseo($datos);
		}

	/*=============================================
	=            Quitar lista de Deseos        =
	=============================================*/

		public $idDeseo;

		public function ajaxQuitarDeseo(){

			$datos = $this->idDeseo;

			$respuesta = ControladorUsuarios::ctrQuitarDeseo($datos);

			echo $respuesta;
		}

	}

	/*=============================================
	=            REGISTRO CON FACEBOOK         =
	=============================================*/

	if(isset($_POST["email"])){

		$regFacebook = new AjaxUsuarios();
		$regFacebook -> email = $_POST["email"];
		$regFacebook -> nombre = $_POST["nombre"];
		$regFacebook -> foto = $_POST["foto"];
		$regFacebook -> ajaxRegistroFacebook();

	}

	/*=============================================
	=            VALIDAR EMAIL EXISTENTE          =
	=============================================*/

	if(isset($_POST["validarEmail"])){

		$valEmail = new AjaxUsuarios();
		$valEmail -> validarEmail = $_POST["validarEmail"];
		$valEmail -> ajaxValidarEmail();

	}

	/*=============================================
	=            Agregar a lista de Deseos        =
	=============================================*/

	if(isset($_POST["idUsuario"])){

		$deseo = new AjaxUsuarios();
		$deseo -> idUsuario = $_POST["idUsuario"];
		$deseo -> idProducto = $_POST["idProducto"];
		$deseo -> ajaxAgregarDeseo();

	}

	/*=============================================
	=            Quitar lista de Deseos        =
	=============================================*/

	if(isset($_POST["idDeseo"])){

		$quitarDeseo = new AjaxUsuarios();
		$quitarDeseo -> idDeseo = $_POST["idDeseo"];
		$quitarDeseo -> ajaxQuitarDeseo();

	}
	
